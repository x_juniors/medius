﻿using System.Collections.Generic;
using System.Data.Entity;

namespace Medius.Common
{
    public interface IRepository<TModel>
        where TModel : class, new()
    {
        bool Exist(params object[] keyValues);

        TModel Get(params object[] keyValues);

        IEnumerable<TModel> Get();

        IEnumerable<TModel> Get(int limit, int offset);

        TModel Add(TModel model);

        int Update(TModel model);

        TModel Remove(TModel model);

        TModel Remove(params object[] keyValues);

        TModel Add(DbContext context, TModel model);

        void Update(DbContext context, TModel model);

        TModel Remove(DbContext context, TModel model);

        TModel Remove(DbContext context, params object[] keyValues);
    }
}
