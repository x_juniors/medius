﻿using System.Collections.Generic;

namespace Medius.Common
{
    public class Service<TModel> : IService<TModel>
        where TModel : class, new()
    {
        protected IRepository<TModel> Repository { get; private set; }

        public Service(IRepository<TModel> repository)
        {
            this.Repository = repository;
        }

        #region IService

        public bool Exist(params object[] keyValues)
        {
            return this.Repository.Exist(keyValues);
        }

        public IEnumerable<TModel> Get()
        {
            return this.Repository.Get();
        }

        public TModel Get(params object[] keyValues)
        {
            return this.Repository.Get(keyValues);
        }

        public IEnumerable<TModel> Get(int limit, int offset)
        {
            return this.Repository.Get(limit, offset);
        }

        public TModel Add(TModel model)
        {
            return this.Repository.Add(model);
        }

        public int Update(TModel model)
        {
            return this.Repository.Update(model);
        }

        public TModel Remove(TModel model)
        {
            return this.Repository.Remove(model);
        }

        public TModel Remove(params object[] keyValues)
        {
            return this.Repository.Remove(keyValues);
        }

        #endregion
    }
}
