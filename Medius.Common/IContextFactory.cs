﻿using System.Data.Entity;

namespace Medius.Common
{
    public interface IContextFactory
    {
        DbContext CreateContext();
    }
}
