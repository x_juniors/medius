﻿using System.Collections.Generic;

namespace Medius.Common
{
    public interface IService<TModel>
    {
        bool Exist(params object[] keyValues);

        TModel Get(params object[] keyValues);

        IEnumerable<TModel> Get();

        IEnumerable<TModel> Get(int limit, int offset);

        TModel Add(TModel model);

        int Update(TModel model);

        TModel Remove(TModel model);

        TModel Remove(params object[] keyValues);
    }
}
