﻿using System.Security.Cryptography;
using System.Text;

namespace Medius.Common
{
    public class SHA512EncryptionService : IEncryptionService
    {
        public string Encrypt(string data)
        {
            var sha512 = new SHA512Managed();
            byte[] dataArray = Encoding.UTF8.GetBytes(data);
            byte[] hashArray = sha512.ComputeHash(dataArray);
            string hash = Encoding.UTF8.GetString(hashArray);
            return hash;
        }
    }
}
