﻿using AutoMapper;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Medius.Common
{
    public class MappableRepository<TEntity, TModel> : IRepository<TModel>
        where TEntity : class, new()
        where TModel : class, new()
    {
        protected IMappingEngine mapper;
        protected IContextFactory ContextFactory { get; set; }

        public MappableRepository(IMappingEngine mapper, IContextFactory contextFactory)
        {
            this.mapper = mapper;
            this.ContextFactory = contextFactory;
        }

        #region __GETTING__

        public TModel Get(params object[] keyValues)
        {
            using (var context = this.ContextFactory.CreateContext())
            {
                var entity = this.GetEntity(context, keyValues);
                if (entity == null)
                {
                    return null;
                }
                return this.mapper.Map<TEntity, TModel>(entity);
            }
        }

        public IEnumerable<TModel> Get()
        {
            using (var context = this.ContextFactory.CreateContext())
            {
                var entities = GetEntities(context);
                if (entities == null)
                {
                    return null;
                }
                return this.mapper.Map<IEnumerable<TEntity>, IEnumerable<TModel>>(entities);
            }
        }

        public IEnumerable<TModel> Get(int limit, int offset)
        {
            using (var context = this.ContextFactory.CreateContext())
            {
                var entities = GetEntities(context).Skip(offset).Take(limit);
                if (entities == null)
                {
                    return null;
                }
                return this.mapper.Map<IEnumerable<TEntity>, IEnumerable<TModel>>(entities);
            }
        }

        public bool Exist(params object[] keyValues)
        {
            using (var context = this.ContextFactory.CreateContext())
            {
                return context.Set<TEntity>().Find(keyValues) != null;
            }
        }

        #endregion

        #region __PERSISTING__

        public TModel Add(TModel model)
        {
            using (var context = this.ContextFactory.CreateContext())
            {
                var entity = this.mapper.Map<TModel, TEntity>(model);
                entity = AddEntity(context, entity);
                context.SaveChanges();
                return this.mapper.Map<TEntity, TModel>(entity);
            }
        }

        public TModel Add(DbContext context, TModel model)
        {
            var entity = this.mapper.Map<TModel, TEntity>(model);
            entity = this.AddEntity(context, entity);
            return this.mapper.Map<TEntity, TModel>(entity);
        }

        public int Update(TModel model)
        {
            using (var context = this.ContextFactory.CreateContext())
            {
                this.Update(context, model);
                return context.SaveChanges();
            }
        }

        public void Update(DbContext context, TModel model)
        {
            var entity = this.mapper.Map<TModel, TEntity>(model);
            this.UpdateEntity(context, entity);
        }

        public TModel Remove(TModel model)
        {
            using (var context = this.ContextFactory.CreateContext())
            {
                model = Remove(context, model);
                context.SaveChanges();
                return model;
            }
        }

        public TModel Remove(DbContext context, TModel model)
        {
            var entity = this.mapper.Map<TModel, TEntity>(model);
            entity = RemoveEntity(context, entity);
            return mapper.Map<TEntity, TModel>(entity);
        }

        public TModel Remove(params object[] keyValues)
        {
            using (var context = ContextFactory.CreateContext())
            {
                var entity = RemoveEntity(context, keyValues);
                context.SaveChanges();
                return mapper.Map<TEntity, TModel>(entity);
            }
        }

        public TModel Remove(DbContext context, params object[] keyValues)
        {
            var entity = RemoveEntity(context, keyValues);
            return mapper.Map<TEntity, TModel>(entity);
        }

        #endregion

        #region __EXTRA__

        protected TEntity GetEntity(DbContext context, params object[] keyValues)
        {
            return context.Set<TEntity>().Find(keyValues);
        }

        protected IQueryable<TEntity> GetEntities(DbContext context)
        {
            return context.Set<TEntity>();
        }

        protected TEntity AddEntity(DbContext context, TEntity entity)
        {
            return context.Set<TEntity>().Add(entity);
        }

        protected void UpdateEntity(DbContext context, TEntity entity)
        {
            context.Entry<TEntity>(entity).State = System.Data.Entity.EntityState.Modified;
        }

        protected TEntity RemoveEntity(DbContext context, params object[] keyValues)
        {
            var entity = context.Set<TEntity>().Find(keyValues);
            return RemoveEntity(context, entity);
        }

        protected TEntity RemoveEntity(DbContext context, TEntity entity)
        {
            context.Entry<TEntity>(entity).State = System.Data.Entity.EntityState.Deleted;
            return entity;
        }

        #endregion
    }
}
