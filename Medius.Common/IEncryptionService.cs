﻿
namespace Medius.Common
{
    public interface IEncryptionService
    {
        string Encrypt(string data);
    }
}
