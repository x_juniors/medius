using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class PostModel
    {
        public PostModel()
        {
            //this.Staffs = new HashSet<Staff>();
        }
    
        public int id { get; set; }
        public string title { get; set; }
    
        public virtual ICollection<StaffModel> Staffs { get; set; }
    }
}
