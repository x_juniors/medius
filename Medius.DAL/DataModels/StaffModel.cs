using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class StaffModel
    {
        public StaffModel()
        {
            //this.Posts = new HashSet<Post>();
            //this.Separations = new HashSet<Separation>();
            //this.Workplaces = new HashSet<Workplace>();
        }
    
        public int id { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string patronymic { get; set; }
    
        public virtual AccountModel Account { get; set; }
        public virtual ICollection<PostModel> Posts { get; set; }
        public virtual ICollection<SeparationModel> Separations { get; set; }
        public virtual ICollection<WorkplaceModel> Workplaces { get; set; }
    }
}
