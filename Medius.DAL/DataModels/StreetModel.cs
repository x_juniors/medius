using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class StreetModel
    {
        public StreetModel()
        {
            //this.Locations = new HashSet<Location>();
        }
    
        public int id { get; set; }
        public string title { get; set; }
        public Nullable<int> localityId { get; set; }
    
        public virtual LocalityModel Locality { get; set; }
        public virtual ICollection<LocationModel> Locations { get; set; }
    }
}
