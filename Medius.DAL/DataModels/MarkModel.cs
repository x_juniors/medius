using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class MarkModel
    {
        public int id { get; set; }
        public string performer { get; set; }
        public int timetableId { get; set; }
    
        public virtual TimetableModel Timetable { get; set; }
    }
}
