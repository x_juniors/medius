using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class TimetableModel
    {
        public TimetableModel()
        {
            //this.Marks = new HashSet<Mark>();
        }
    
        public int id { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public int assignmentId { get; set; }
    
        public virtual AssignmentModel Assignment { get; set; }
        public virtual ICollection<MarkModel> Marks { get; set; }
    }
}
