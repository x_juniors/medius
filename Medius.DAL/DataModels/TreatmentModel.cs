using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class TreatmentModel
    {
        public int id { get; set; }
        public string otherTreatment { get; set; }
        public Nullable<byte> otherTreatmentTypes { get; set; }
        public string amount { get; set; }
        public Nullable<byte> result { get; set; }
        public Nullable<byte> restoredEfficiency { get; set; }
        public Nullable<System.DateTime> oncologicalObserving { get; set; }
        public Nullable<System.DateTime> xrayObserving { get; set; }
    
        public virtual MedcardModel Medcard { get; set; }
    }
}
