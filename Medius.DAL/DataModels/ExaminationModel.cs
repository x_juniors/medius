using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class ExaminationModel
    {
        public int id { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string performer { get; set; }
        public Nullable<int> examinationTypeId { get; set; }
        public int medcardId { get; set; }
    
        public virtual ExaminationTypeModel ExaminationType { get; set; }
        public virtual MedcardModel Medcard { get; set; }
    }
}
