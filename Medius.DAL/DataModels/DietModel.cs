using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class DietModel
    {
        public int id { get; set; }
        public string appointmentScheme { get; set; }
        public Nullable<System.DateTime> appointmentDate { get; set; }
        public Nullable<System.DateTime> cancelDate { get; set; }
        public int medcardId { get; set; }
    
        public virtual MedcardModel Medcard { get; set; }
    }
}
