using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class TreatmentDiagnoseModel
    {
        public int id { get; set; }
        public string final { get; set; }
        public string main { get; set; }
        public string complication { get; set; }
        public string related { get; set; }
    
        public virtual MedcardModel Medcard { get; set; }
    }
}
