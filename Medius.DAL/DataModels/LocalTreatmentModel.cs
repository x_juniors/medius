using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class LocalTreatmentModel
    {
        public int id { get; set; }
        public string procedureCode { get; set; }
        public string procedureName { get; set; }
        public Nullable<System.DateTime> appointmentDate { get; set; }
        public Nullable<System.DateTime> cancelDate { get; set; }
    
        public virtual MedcardModel Medcard { get; set; }
    }
}
