using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class LocalityModel
    {
        public LocalityModel()
        {
            //this.Streets = new HashSet<Street>();
        }
    
        public int id { get; set; }
        public string title { get; set; }
        public Nullable<int> regionId { get; set; }
        public Nullable<byte> type { get; set; }
        public string area { get; set; }
    
        public virtual ICollection<StreetModel> Streets { get; set; }
        public virtual RegionModel Region { get; set; }
    }
}
