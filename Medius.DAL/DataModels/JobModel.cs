using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class JobModel
    {
        public JobModel()
        {
            //this.Patients = new HashSet<Patient>();
        }
    
        public int id { get; set; }
        public string post { get; set; }
        public int organizationId { get; set; }
    
        public virtual OrganizationModel Organization { get; set; }
        public virtual ICollection<PatientModel> Patients { get; set; }
    }
}
