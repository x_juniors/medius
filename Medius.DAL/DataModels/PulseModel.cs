using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class PulseModel
    {
        public Nullable<int> value { get; set; }
        public System.DateTime date { get; set; }
        public int medcardId { get; set; }
    
        public virtual MedcardModel Medcard { get; set; }
    }
}
