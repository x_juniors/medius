using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class MedcardModel
    {
        public MedcardModel()
        {
            //this.Assignments = new HashSet<Assignment>();
            //this.Diets = new HashSet<Diet>();
            //this.EfficiencyMarks = new HashSet<EfficiencyMark>();
            //this.Examinations = new HashSet<Examination>();
            //this.ExaminationResults = new HashSet<ExaminationResult>();
            //this.ExpertInspections = new HashSet<ExpertInspection>();
            //this.Notebooks = new HashSet<Notebook>();
            //this.Operations = new HashSet<Operation>();
            //this.Physiotherapies = new HashSet<Physiotherapy>();
            //this.Pressures = new HashSet<Pressure>();
            //this.Psychotherapeutics = new HashSet<Psychotherapeutic>();
            //this.Pulses = new HashSet<Pulse>();
            //this.Temperatures = new HashSet<Temperature>();
        }
    
        public int id { get; set; }
        public string number { get; set; }
        public Nullable<System.DateTime> hospitalizationDate { get; set; }
        public Nullable<int> chamber { get; set; }
        public Nullable<byte> isHospitalizedInCurrentYear { get; set; }
        public string sender { get; set; }
        public Nullable<short> pediculosis { get; set; }
        public Nullable<short> scabInspection { get; set; }
        public Nullable<int> pulseValue { get; set; }
        public Nullable<float> temperatureValue { get; set; }
        public Nullable<int> pressureTop { get; set; }
        public Nullable<int> pressureLower { get; set; }
        public Nullable<System.DateTime> rw { get; set; }
        public Nullable<System.DateTime> hivInfection { get; set; }
        public Nullable<short> rwStatus { get; set; }
        public Nullable<short> hivStatus { get; set; }
        public Nullable<int> separationId { get; set; }
        public int patientId { get; set; }
        public Nullable<System.DateTime> hepatitisDate { get; set; }
        public Nullable<short> hepatitisStatus { get; set; }
        public Nullable<int> contractId { get; set; }
    
        public virtual AdmissionDepartmentModel AdmissionDepartment { get; set; }
        public virtual ICollection<AssignmentModel> Assignments { get; set; }
        public virtual ContractModel Contract { get; set; }
        public virtual ICollection<DietModel> Diets { get; set; }
        public virtual ICollection<EfficiencyMarkModel> EfficiencyMarks { get; set; }
        public virtual ICollection<ExaminationModel> Examinations { get; set; }
        public virtual ICollection<ExaminationResultModel> ExaminationResults { get; set; }
        public virtual ICollection<ExpertInspectionModel> ExpertInspections { get; set; }
        public virtual FoodModeModel FoodMode { get; set; }
        public virtual HospitalizationModel Hospitalization { get; set; }
        public virtual LocalTreatmentModel LocalTreatment { get; set; }
        public virtual SeparationModel Separation { get; set; }
        public virtual ICollection<NotebookModel> Notebooks { get; set; }
        public virtual ICollection<OperationModel> Operations { get; set; }
        public virtual ICollection<PhysiotherapyModel> Physiotherapies { get; set; }
        public virtual ICollection<PressureModel> Pressures { get; set; }
        public virtual ICollection<PsychotherapeuticModel> Psychotherapeutics { get; set; }
        public virtual ICollection<PulseModel> Pulses { get; set; }
        public virtual ICollection<TemperatureModel> Temperatures { get; set; }
        public virtual TreatmentModel Treatment { get; set; }
        public virtual TreatmentDiagnoseModel TreatmentDiagnose { get; set; }
        public virtual PatientModel Patient { get; set; }
    }
}
