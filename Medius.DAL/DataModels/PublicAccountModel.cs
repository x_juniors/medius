using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public class PublicAccountModel
    {
        public PublicAccountModel()
        {
            //this.Tokens = new HashSet<Token>();
        }

        public int id { get; set; }
        public string login { get; set; }
        public int roleId { get; set; }

        public virtual RoleModel Role { get; set; }
        public virtual StaffModel Staff { get; set; }
        public virtual ICollection<TokenModel> Tokens { get; set; }
    }
}
