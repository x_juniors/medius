using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class ExaminationTypeModel
    {
        public ExaminationTypeModel()
        {
            //this.Examinations = new HashSet<Examination>();
        }
    
        public int id { get; set; }
        public string type { get; set; }
    
        public virtual ICollection<ExaminationModel> Examinations { get; set; }
    }
}
