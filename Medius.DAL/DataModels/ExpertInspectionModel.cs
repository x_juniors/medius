using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class ExpertInspectionModel
    {
        public int id { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string note { get; set; }
        public string author { get; set; }
        public int medcardId { get; set; }
    
        public virtual MedcardModel Medcard { get; set; }
    }
}
