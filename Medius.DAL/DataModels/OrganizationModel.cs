using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class OrganizationModel
    {
        public OrganizationModel()
        {
            //this.Jobs = new HashSet<Job>();
        }
    
        public int id { get; set; }
        public string reduction { get; set; }
        public string title { get; set; }
        public string address { get; set; }
    
        public virtual ICollection<JobModel> Jobs { get; set; }
    }
}
