using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class IcdClassModel
    {
        public IcdClassModel()
        {
            //this.Blocks = new HashSet<Block>();
        }
    
        public int id { get; set; }
        public string title { get; set; }
    
        public virtual ICollection<BlockModel> Blocks { get; set; }
    }
}
