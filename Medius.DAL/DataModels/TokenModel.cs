using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class TokenModel
    {
        public Guid value { get; set; }
        public Nullable<DateTime> expiresIn { get; set; }
        public string ipAddress { get; set; }
        public string userAgent { get; set; }
        public int accountId { get; set; }
    
        public virtual AccountModel Account { get; set; }
    }
}
