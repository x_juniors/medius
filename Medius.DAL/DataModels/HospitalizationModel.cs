using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class HospitalizationModel
    {
        public int id { get; set; }
        public Nullable<int> afterHours { get; set; }
        public Nullable<byte> hospitalizationPriority { get; set; }
        public string institutionDiagnosis { get; set; }
        public string hospitalizationDiagnosis { get; set; }
        public string clinicalDiagnosis { get; set; }
        public Nullable<System.DateTime> settingDate { get; set; }
    
        public virtual MedcardModel Medcard { get; set; }
    }
}
