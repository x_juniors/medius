using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class PsychotherapeuticModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string performer { get; set; }
        public int medcardId { get; set; }
    
        public virtual MedcardModel Medcard { get; set; }
    }
}
