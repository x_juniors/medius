using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class TemperatureModel
    {
        public Nullable<float> value { get; set; }
        public System.DateTime date { get; set; }
        public int medcardId { get; set; }
    
        public virtual MedcardModel Medcard { get; set; }
    }
}
