using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class FoodModel
    {
        public FoodModel()
        {
            //this.FoodModes = new HashSet<FoodMode>();
        }
    
        public int id { get; set; }
        public string title { get; set; }
    
        public virtual ICollection<FoodModeModel> FoodModes { get; set; }
    }
}
