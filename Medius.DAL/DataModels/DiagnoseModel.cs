using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class DiagnoseModel
    {
        public string id { get; set; }
        public string number { get; set; }
        public string title { get; set; }
        public string blockId { get; set; }
    
        public virtual BlockModel Block { get; set; }
    }
}
