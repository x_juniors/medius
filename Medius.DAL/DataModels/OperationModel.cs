using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class OperationModel
    {
        public int id { get; set; }
        public Nullable<int> number { get; set; }
        public string title { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string anesthesiaMethod { get; set; }
        public string complications { get; set; }
        public string surgeon { get; set; }
        public string anesthetist { get; set; }
        public int medcardId { get; set; }
    
        public virtual MedcardModel Medcard { get; set; }
    }
}
