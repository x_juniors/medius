using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class EfficiencyMarkModel
    {
        public int id { get; set; }
        public string number { get; set; }
        public Nullable<System.DateTime> begin { get; set; }
        public Nullable<System.DateTime> end { get; set; }
        public int medcardId { get; set; }
    
        public virtual MedcardModel Medcard { get; set; }
    }
}
