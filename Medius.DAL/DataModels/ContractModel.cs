using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class ContractModel
    {
        public ContractModel()
        {
            //this.Medcards = new HashSet<Medcard>();
        }
    
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string subject { get; set; }
    
        public virtual ICollection<MedcardModel> Medcards { get; set; }
    }
}
