using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class FoodModeModel
    {
        public int id { get; set; }
        public Nullable<System.DateTime> from { get; set; }
        public Nullable<System.DateTime> to { get; set; }
        public Nullable<int> foodId { get; set; }
    
        public virtual FoodModel Food { get; set; }
        public virtual MedcardModel Medcard { get; set; }
    }
}
