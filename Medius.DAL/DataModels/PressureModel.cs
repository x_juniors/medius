using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class PressureModel
    {
        public int id { get; set; }
        public Nullable<int> top { get; set; }
        public Nullable<int> lower { get; set; }
        public System.DateTime date { get; set; }
        public int medcardId { get; set; }
    
        public virtual MedcardModel Medcard { get; set; }
    }
}
