﻿
namespace Medius.DAL.DataModels
{
    public class PasswordModel
    {
        public int id { get; set; }
        public string newPassword { get; set; }
        public string currentPassword { get; set; }
    }
}
