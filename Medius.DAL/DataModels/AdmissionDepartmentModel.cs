using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class AdmissionDepartmentModel
    {
        public int id { get; set; }
        public string complaint { get; set; }
        public string diseaseAnamnesis { get; set; }
        public string lifeAnamnesis { get; set; }
        public string objectiveState { get; set; }
        public string other { get; set; }
    
        public virtual MedcardModel Medcard { get; set; }
    }
}
