using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class CategoryModel
    {
        public CategoryModel()
        {
            //this.Patients = new HashSet<Patient>();
        }
    
        public int id { get; set; }
        public string title { get; set; }
    
        public virtual ICollection<PatientModel> Patients { get; set; }
    }
}
