using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class RegionModel
    {
        public RegionModel()
        {
            //this.Localities = new HashSet<Locality>();
        }
    
        public int id { get; set; }
        public string title { get; set; }
    
        public virtual ICollection<LocalityModel> Localities { get; set; }
    }
}
