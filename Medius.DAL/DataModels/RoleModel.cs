using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class RoleModel
    {
        public RoleModel()
        {
            //this.Accounts = new HashSet<Account>();
            //this.ChildRoles = new HashSet<Role>();
        }
    
        public int id { get; set; }
        public Nullable<int> parentId { get; set; }
        public string roleTitle { get; set; }
    
        public virtual ICollection<AccountModel> Accounts { get; set; }
        public virtual ICollection<RoleModel> ChildRoles { get; set; }
        public virtual RoleModel ParentRole { get; set; }
    }
}
