using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class PatientModel
    {
        public PatientModel()
        {
            //this.Locations = new HashSet<Location>();
            //this.Medcards = new HashSet<Medcard>();
        }
    
        public int id { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string patronymic { get; set; }
        public Nullable<byte> sex { get; set; }
        public Nullable<System.DateTime> born { get; set; }
        public Nullable<byte> bloodType { get; set; }
        public Nullable<byte> rhFactor { get; set; }
        public string sensitivity { get; set; }
        public Nullable<int> jobId { get; set; }
        public string phone { get; set; }
        public Nullable<int> categoryId { get; set; }
    
        public virtual CategoryModel Category { get; set; }
        public virtual JobModel Job { get; set; }
        public virtual ICollection<LocationModel> Locations { get; set; }
        public virtual ICollection<MedcardModel> Medcards { get; set; }
    }
}
