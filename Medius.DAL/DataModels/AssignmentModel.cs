using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class AssignmentModel
    {
        public AssignmentModel()
        {
            //this.Timetables = new HashSet<Timetable>();
        }
    
        public int id { get; set; }
        public string type { get; set; }
        public int medcardId { get; set; }
    
        public virtual MedcardModel Medcard { get; set; }
        public virtual ICollection<TimetableModel> Timetables { get; set; }
    }
}
