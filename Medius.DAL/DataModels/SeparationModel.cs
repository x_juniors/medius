using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class SeparationModel
    {
        public SeparationModel()
        {
            //this.Medcards = new HashSet<Medcard>();
            //this.Staffs = new HashSet<Staff>();
        }
    
        public int id { get; set; }
        public string title { get; set; }
    
        public virtual ICollection<MedcardModel> Medcards { get; set; }
        public virtual ICollection<StaffModel> Staffs { get; set; }
    }
}
