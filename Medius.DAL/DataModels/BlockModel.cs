using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class BlockModel
    {
        public BlockModel()
        {
            //this.Diagnoses = new HashSet<Diagnose>();
        }
    
        public string id { get; set; }
        public string title { get; set; }
        public Nullable<int> icdClassId { get; set; }
    
        public virtual IcdClassModel IcdClass { get; set; }
        public virtual ICollection<DiagnoseModel> Diagnoses { get; set; }
    }
}
