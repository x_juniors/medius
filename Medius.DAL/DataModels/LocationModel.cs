using System;
using System.Collections.Generic;

namespace Medius.DAL.DataModels
{
    public  class LocationModel
    {
        public int id { get; set; }
        public Nullable<int> streetId { get; set; }
        public string apartment { get; set; }
        public string note { get; set; }
        public Nullable<int> patientId { get; set; }
    
        public virtual StreetModel Street { get; set; }
        public virtual PatientModel Patient { get; set; }
    }
}
