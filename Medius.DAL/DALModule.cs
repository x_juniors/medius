﻿using Autofac;
using AutoMapper;
using Medius.Common;
using Medius.DAL.Repositories;

namespace Medius.DAL
{
    public class DALModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Mapper.AddProfile<MediusMapperProfile>();
            builder.RegisterInstance(Mapper.Engine).As<IMappingEngine>();
            builder.RegisterType<MediusContextFactory>().As<IContextFactory>();
            builder.RegisterType<MediusContextFactory>().AsSelf();
            // Repositories
            builder.RegisterType<PatientRepository>().As<IPatientRepository>();
            builder.RegisterType<CategoryRepository>().As<ICategoryRepository>();
            builder.RegisterType<JobRepository>().As<IJobRepository>();
            builder.RegisterType<OrganizationRepository>().As<IOrganizationRepository>();

            builder.RegisterType<AccountRepository>().As<IAccountRepository>();
            builder.RegisterType<PublicAccountRepository>().As<IPublicAccountRepository>();
            builder.RegisterType<RoleRepository>().As<IRoleRepository>();
            builder.RegisterType<TokenRepository>().As<ITokenRepository>();
        }
    }
}
