﻿using Medius.Common;
using Medius.DAL.EDM;
using System.Data.Entity;

namespace Medius.DAL
{
    public class MediusContextFactory : IContextFactory
    {
        public MediusContext CreateMediusContext()
        {
            var context = new MediusContext();
            return context;
        }

        public DbContext CreateContext()
        {
            return this.CreateMediusContext();
        }
    }
}
