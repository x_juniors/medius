﻿using Medius.DAL.DataModels;
using Medius.DAL.EDM;
using AutoMapper;
using Medius.DAL.Extensions;

namespace Medius.DAL
{
    public class MediusMapperProfile : Profile
    {
        protected override void Configure()
        {
            this.AllowNullCollections = true;
            // Patient
            this.CreateMap<Patient, PatientModel>()
                .Ignore(m => m.Category)
                .Ignore(m => m.Job)
                .Ignore(m => m.Locations)
                .Ignore(m => m.Medcards)
            .ReverseMap()
                .Ignore(m => m.Category)
                .Ignore(m => m.Job)
                .Ignore(m => m.Locations)
                .Ignore(m => m.Medcards);
            // Category
            this.CreateMap<Category, CategoryModel>()
                .Ignore(m => m.Patients)
            .ReverseMap()
                .Ignore(m => m.Patients);
            // Job
            this.CreateMap<Job, JobModel>()
                .Ignore(m => m.Patients)
                .Ignore(m => m.Organization)
            .ReverseMap()
                .Ignore(m => m.Patients)
                .Ignore(m => m.Organization);
            // Organization
            this.CreateMap<Organization, OrganizationModel>()
                .Ignore(m => m.Jobs)
            .ReverseMap()
                .Ignore(m => m.Jobs);
            // Account
            this.CreateMap<Account, AccountModel>()
                .Ignore(m => m.Staff)
                .Ignore(m => m.Tokens)
                //.Ignore(m => m.Role)
            .ReverseMap()
                .Ignore(m => m.Staff)
                .Ignore(m => m.Tokens)
                .Ignore(m => m.Role);
            // PublicAccount
            this.CreateMap<Account, PublicAccountModel>()
                .Ignore(m => m.Staff)
                .Ignore(m => m.Tokens)
                //.Ignore(m => m.Role)
            .ReverseMap()
                .Ignore(m => m.Staff)
                .Ignore(m => m.Tokens)
                .Ignore(m => m.Role);
            // PublicAccount
            this.CreateMap<PasswordModel, Account>()
                .ForMember(d => d.passwordHash, opt => opt.MapFrom(s => s.currentPassword));
            // Token
            this.CreateMap<Token, TokenModel>()
                .Ignore(m => m.Account)
            .ReverseMap()
                .Ignore(m => m.Account);
            // Role
            this.CreateMap<Role, RoleModel>()
                .Ignore(m => m.Accounts)
                .Ignore(m => m.ChildRoles)
                .Ignore(m => m.ParentRole)
            .ReverseMap()
                .Ignore(m => m.Accounts)
                .Ignore(m => m.ChildRoles)
                .Ignore(m => m.ParentRole);
        }
    }
}
