﻿using AutoMapper;
using Medius.Common;

namespace Medius.DAL.Repositories
{
    public class MediusRepository<TEntity, TModel> : MappableRepository<TEntity, TModel>
        where TEntity : class, new()
        where TModel : class, new()
    {
        protected new MediusContextFactory ContextFactory
        {
            get { return base.ContextFactory as MediusContextFactory; }
        }

        public MediusRepository(IMappingEngine mapper, MediusContextFactory contextFactory)
            : base(mapper, contextFactory) { }
    }
}
