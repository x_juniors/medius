﻿using AutoMapper;
using Medius.DAL.DataModels;
using Medius.DAL.EDM;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Medius.DAL.Repositories
{
    public class TokenRepository : MediusRepository<Token, TokenModel>, ITokenRepository
    {
        public TokenRepository(IMappingEngine mapper, MediusContextFactory contextFactory)
            : base(mapper, contextFactory) { }

        public TokenModel ClearAllExcept(int accountId, Guid tokenValue)
        {
            using (var context = this.ContextFactory.CreateMediusContext())
            {
                var tokens = context.Accounts.Find(accountId).Tokens.Where(p => p.value != tokenValue);
                context.Tokens.RemoveRange(tokens);
                context.SaveChanges();
                var token = context.Tokens.Find(tokenValue);
                return this.mapper.Map<Token, TokenModel>(token);
            }
        }

        public IEnumerable<TokenModel> Get(int accountId)
        {
            using (var context = ContextFactory.CreateMediusContext())
            {
                var entities = GetEntities(context).Where(p => p.accountId == accountId);
                return mapper.Map<IEnumerable<Token>, IEnumerable<TokenModel>>(entities);
            }
        }
    }
}
