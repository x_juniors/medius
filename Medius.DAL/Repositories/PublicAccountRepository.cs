﻿using AutoMapper;
using Medius.DAL.DataModels;
using Medius.DAL.EDM;

namespace Medius.DAL.Repositories
{
    public class PublicAccountRepository : MediusRepository<Account, PublicAccountModel>, IPublicAccountRepository
    {
        public PublicAccountRepository(IMappingEngine mapper, MediusContextFactory contextFactory)
            : base(mapper, contextFactory) { }

        public new int Update(PublicAccountModel model)
        {
            using (var context = ContextFactory.CreateMediusContext())
            {
                var account = context.Accounts.Find(model.id);
                account.login = model.login;
                account.roleId = model.roleId;
                return context.SaveChanges();
            }
        }
    }
}
