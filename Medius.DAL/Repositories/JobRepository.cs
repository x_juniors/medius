﻿using AutoMapper;
using Medius.DAL.DataModels;
using Medius.DAL.EDM;

namespace Medius.DAL.Repositories
{
    public class JobRepository : MediusRepository<Job, JobModel>, IJobRepository
    {
        public JobRepository(IMappingEngine mapper, MediusContextFactory contextFactory)
            : base(mapper, contextFactory) { }

        public JobModel GetByPatient(int patientId)
        {
            using (var context = this.ContextFactory.CreateMediusContext())
            {
                var entity = context.Patients.Find(patientId).Job;
                return this.mapper.Map<Job, JobModel>(entity);
            }
        }
    }
}
