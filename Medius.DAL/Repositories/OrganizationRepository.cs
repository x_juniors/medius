﻿using AutoMapper;
using Medius.DAL.DataModels;
using Medius.DAL.EDM;

namespace Medius.DAL.Repositories
{
    public class OrganizationRepository : MediusRepository<Organization, OrganizationModel>, IOrganizationRepository
    {
        public OrganizationRepository(IMappingEngine mapper, MediusContextFactory contextFactory)
            : base(mapper, contextFactory) { }

        public OrganizationModel GetByPatient(int patientId)
        {
            using (var context = this.ContextFactory.CreateMediusContext())
            {
                var job = context.Patients.Find(patientId).Job;
                if (job == null)
                {
                    return null;
                }
                var entity = context.Patients.Find(patientId).Job.Organization;
                return this.mapper.Map<Organization, OrganizationModel>(entity);
            }
        }
    }
}
