﻿using AutoMapper;
using Medius.DAL.DataModels;
using Medius.DAL.EDM;

namespace Medius.DAL.Repositories
{
    public class PatientRepository : MediusRepository<Patient, PatientModel>, IPatientRepository
    {
        public PatientRepository(IMappingEngine mapper, MediusContextFactory contextFactory)
            : base(mapper, contextFactory) { }
    }
}
