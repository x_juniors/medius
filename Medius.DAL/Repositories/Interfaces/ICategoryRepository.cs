﻿using Medius.Common;
using Medius.DAL.DataModels;

namespace Medius.DAL.Repositories
{
    public interface ICategoryRepository : IRepository<CategoryModel>
    {
        CategoryModel GetByPatient(int patientId);
    }
}
