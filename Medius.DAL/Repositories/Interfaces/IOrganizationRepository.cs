﻿using Medius.Common;
using Medius.DAL.DataModels;

namespace Medius.DAL.Repositories
{
    public interface IOrganizationRepository : IRepository<OrganizationModel>
    {
        OrganizationModel GetByPatient(int patientId);
    }
}
