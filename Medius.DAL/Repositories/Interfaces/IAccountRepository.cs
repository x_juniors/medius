﻿using Medius.Common;
using Medius.DAL.DataModels;

namespace Medius.DAL.Repositories
{
    public interface IAccountRepository : IRepository<AccountModel>
    {
        AccountModel Get(TokenModel token);

        AccountModel Get(string login, string passwordHash);

        void ChangePassword(PasswordModel password);
    }
}
