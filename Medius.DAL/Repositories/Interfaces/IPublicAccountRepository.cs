﻿using Medius.Common;
using Medius.DAL.DataModels;

namespace Medius.DAL.Repositories
{
    public interface IPublicAccountRepository : IRepository<PublicAccountModel>
    {
        int Update(PublicAccountModel model);
    }
}
