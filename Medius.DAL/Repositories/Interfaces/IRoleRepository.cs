﻿using Medius.Common;
using Medius.DAL.DataModels;
using System.Collections.Generic;

namespace Medius.DAL.Repositories
{
    public interface IRoleRepository : IRepository<RoleModel>
    {
        IEnumerable<RoleModel> Get(AccountModel account);
    }
}
