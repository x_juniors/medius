﻿using Medius.Common;
using Medius.DAL.DataModels;

namespace Medius.DAL.Repositories
{
    public interface IPatientRepository : IRepository<PatientModel>
    {

    }
}
