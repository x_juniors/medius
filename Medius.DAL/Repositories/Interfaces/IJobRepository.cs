﻿using Medius.Common;
using Medius.DAL.DataModels;

namespace Medius.DAL.Repositories
{
    public interface IJobRepository : IRepository<JobModel>
    {
        JobModel GetByPatient(int patientId);
    }
}
