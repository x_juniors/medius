﻿using Medius.Common;
using Medius.DAL.DataModels;
using System;
using System.Collections.Generic;

namespace Medius.DAL.Repositories
{
    public interface ITokenRepository : IRepository<TokenModel>
    {
        TokenModel ClearAllExcept(int accountId, Guid tokenValue);

        IEnumerable<TokenModel> Get(int accountId);
    }
}
