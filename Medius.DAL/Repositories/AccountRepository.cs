﻿using AutoMapper;
using Medius.DAL.DataModels;
using Medius.DAL.EDM;
using System;
using System.Linq;

namespace Medius.DAL.Repositories
{
    public class AccountRepository : MediusRepository<Account, AccountModel>, IAccountRepository
    {
        public AccountRepository(IMappingEngine mapper, MediusContextFactory contextFactory)
            : base(mapper, contextFactory) { }

        public AccountModel Get(TokenModel token)
        {
            using (var context = this.ContextFactory.CreateMediusContext())
            {
                var tokenEntity = context.Tokens.Find(token.value);
                if (tokenEntity.expiresIn.Value < DateTime.Now)
                {
                    return null;
                }
                var accountEntity = tokenEntity.Account;
                var account = this.mapper.Map<Account, AccountModel>(accountEntity);
                return account;
            }
        }

        public AccountModel Get(string login, string passwordHash)
        {
            using (var context = ContextFactory.CreateMediusContext())
            {
                AccountModel accountModel = null;
                var account = context.Accounts.FirstOrDefault(p => p.login.Equals(login) && p.passwordHash.Equals(passwordHash));
                if (account != null)
                {
                    accountModel = this.mapper.Map<Account, AccountModel>(account);
                }
                return accountModel;
            }
        }

        public void ChangePassword(PasswordModel password)
        {
            using (var context = ContextFactory.CreateMediusContext())
            {
                var account = context.Accounts.Find(password.id);
                account.passwordHash = password.currentPassword;
                UpdateEntity(context, account);
                context.SaveChanges();
            }
        }
    }
}
