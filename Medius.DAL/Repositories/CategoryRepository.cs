﻿using AutoMapper;
using Medius.DAL.DataModels;
using Medius.DAL.EDM;

namespace Medius.DAL.Repositories
{
    public class CategoryRepository : MediusRepository<Category, CategoryModel>, ICategoryRepository
    {
        public CategoryRepository(IMappingEngine mapper, MediusContextFactory contextFactory)
            : base(mapper, contextFactory) { }

        public CategoryModel GetByPatient(int patientId)
        {
            using (var context = this.ContextFactory.CreateMediusContext())
            {
                var entity = context.Patients.Find(patientId).Category;
                return this.mapper.Map<Category, CategoryModel>(entity);
            }
        }
    }
}
