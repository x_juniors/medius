﻿using AutoMapper;
using Medius.DAL.DataModels;
using Medius.DAL.EDM;
using System.Collections.Generic;

namespace Medius.DAL.Repositories
{
    public class RoleRepository : MediusRepository<Role, RoleModel>, IRoleRepository
    {
        public RoleRepository(IMappingEngine mapper, MediusContextFactory contextFactory)
            : base(mapper, contextFactory) { }

        public IEnumerable<RoleModel> Get(AccountModel account)
        {
            using (var context = ContextFactory.CreateMediusContext())
            {
                var accountEntity = context.Accounts.Find(account.id);
                var eRoles = this.CollectRoles(accountEntity.Role);
                var roles = mapper.Map<IEnumerable<Role>, IEnumerable<RoleModel>>(eRoles);
                return roles;
            }
        }

        private IEnumerable<Role> CollectRoles(Role role)
        {
            var roles = new List<Role>();
            roles.Add(role);
            _CollectRoles(role, ref roles);
            return roles;
        }

        private void _CollectRoles(Role role, ref List<Role> roles)
        {
            if (role != null)
            {
                if (role.ChildRoles != null)
                {
                    foreach (var item in role.ChildRoles)
                    {
                        roles.Add(item);
                        _CollectRoles(item, ref roles);
                    }
                }
            }
        }
    }
}
