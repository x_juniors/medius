//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Medius.DAL.EDM
{
    using System;
    using System.Collections.Generic;
    
    public partial class Locality
    {
        public Locality()
        {
            this.Streets = new HashSet<Street>();
        }
    
        public int id { get; set; }
        public string title { get; set; }
        public Nullable<int> regionId { get; set; }
        public Nullable<byte> type { get; set; }
        public string area { get; set; }
    
        public virtual ICollection<Street> Streets { get; set; }
        public virtual Region Region { get; set; }
    }
}
