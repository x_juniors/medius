//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Medius.DAL.EDM
{
    using System;
    using System.Collections.Generic;
    
    public partial class Operation
    {
        public int id { get; set; }
        public Nullable<int> number { get; set; }
        public string title { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string anesthesiaMethod { get; set; }
        public string complications { get; set; }
        public string surgeon { get; set; }
        public string anesthetist { get; set; }
        public int medcardId { get; set; }
    
        public virtual Medcard Medcard { get; set; }
    }
}
