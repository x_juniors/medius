//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Medius.DAL.EDM
{
    using System;
    using System.Collections.Generic;
    
    public partial class Pressure
    {
        public Nullable<int> top { get; set; }
        public Nullable<int> lower { get; set; }
        public System.DateTime date { get; set; }
        public int medcardId { get; set; }
    
        public virtual Medcard Medcard { get; set; }
    }
}
