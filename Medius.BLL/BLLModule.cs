﻿using Autofac;
using Medius.BLL.Services;
using Medius.DAL;
using Medius.Common;

namespace Medius.BLL
{
    public class BLLModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<DALModule>();
            // Services
            //builder.RegisterType<SHA512EncryptionService>().As<IEncryptionService>();
            builder.RegisterType<FakeEncryptionService>().As<IEncryptionService>();
            builder.RegisterType<PatientService>().As<IPatientService>();
            builder.RegisterType<AccountService>().As<IAccountService>();
        }
    }
}
