﻿using Medius.Common;
using Medius.DAL.DataModels;
using Medius.DAL.Repositories;

namespace Medius.BLL.Services
{
    public class PatientService : Service<PatientModel>, IPatientService
    {
        protected new IPatientRepository Repository
        {
            get { return base.Repository as IPatientRepository; }
        }
        protected ICategoryRepository categoryRepository;
        protected IJobRepository jobRepository;
        protected IOrganizationRepository organizationRepository;

        public PatientService(IPatientRepository repository, ICategoryRepository categoryRepository,
            IJobRepository jobRepository, IOrganizationRepository organizationRepository)
            : base(repository)
        {
            this.categoryRepository = categoryRepository;
            this.jobRepository = jobRepository;
            this.organizationRepository = organizationRepository;
        }

        public CategoryModel GetCategory(int patientId)
        {
            return this.categoryRepository.GetByPatient(patientId);
        }

        public JobModel GetJob(int patientId)
        {
            return this.jobRepository.GetByPatient(patientId);
        }

        public OrganizationModel GetOrganization(int patientId)
        {
            return this.organizationRepository.GetByPatient(patientId);
        }
    }
}
