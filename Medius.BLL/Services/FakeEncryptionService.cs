﻿using Medius.Common;

namespace Medius.BLL.Services
{
    public class FakeEncryptionService : IEncryptionService
    {
        public string Encrypt(string data)
        {
            return data;
        }
    }
}