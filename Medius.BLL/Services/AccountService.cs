﻿using Medius.Common;
using Medius.DAL.DataModels;
using Medius.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Medius.BLL.Services
{
    public class AccountService : Service<PublicAccountModel>, IAccountService
    {
        private const double TOKEN_LIFETIME_IN_DAYS = 7;

        protected new IPublicAccountRepository Repository
        {
            get { return base.Repository as IPublicAccountRepository; }
            set { this.Repository = value; }
        }
        private IRoleRepository _roleRepository;
        private ITokenRepository _tokenRepository;
        private IEncryptionService _encryptionService;
        private IAccountRepository _accountRepository;

        public AccountService(IPublicAccountRepository repository, IEncryptionService encryptionService, IRoleRepository roleRepository,
            ITokenRepository tokenRepository, IAccountRepository accountRepository)
            : base(repository)
        {
            _encryptionService = encryptionService;
            _roleRepository = roleRepository;
            _tokenRepository = tokenRepository;
            _accountRepository = accountRepository;
        }

        public AccountModel Register(AccountModel account)
        {
            account.passwordHash = _encryptionService.Encrypt(account.passwordHash);
            return _accountRepository.Add(account);
        }

        public void ChangePassword(PasswordModel password)
        {
            password.currentPassword = _encryptionService.Encrypt(password.newPassword);
            _accountRepository.ChangePassword(password);
        }

        public TokenModel AddToken(TokenModel token, string login, string password)
        {
            string hash = _encryptionService.Encrypt(password);
            var account = _accountRepository.Get(login, hash);
            if (account != null)
            {
                token.accountId = account.id;
                token.expiresIn = DateTime.Now.AddDays(AccountService.TOKEN_LIFETIME_IN_DAYS);
                token = _tokenRepository.Add(token);
            }
            return token;
        }

        public AccountModel GetByToken(TokenModel token)
        {
            return _accountRepository.Get(token);
        }

        public IEnumerable<RoleModel> GetRoles(AccountModel account)
        {
            return _roleRepository.Get(account);
        }

        public TokenModel ClearAllExcept(int accountId, Guid tokenValue)
        {
            return _tokenRepository.ClearAllExcept(accountId, tokenValue);
        }

        public TokenModel RemoveToken(Guid tokenValue)
        {
            return _tokenRepository.Remove(tokenValue);
        }

        public AccountModel GetByTokenValue(Guid tokenValue)
        {
            var token = _tokenRepository.Get(tokenValue);
            if (token == null)
            {
                return null;
            }
            return _accountRepository.Get(token);
        }

        public IEnumerable<TokenModel> GetAllTokens(int accountId)
        {
            var tokens = _tokenRepository.Get(accountId).ToList();
            tokens.Sort((t1, t2) => (t1.expiresIn.Value.CompareTo(t2.expiresIn.Value) * -1));
            tokens.ForEach(a => a.value = Guid.Empty);
            return tokens;
        }
    }
}
