﻿using Medius.Common;
using Medius.DAL.DataModels;

namespace Medius.BLL.Services
{
    public interface IPatientService : IService<PatientModel>
    {
        CategoryModel GetCategory(int patientId);

        JobModel GetJob(int patientId);

        OrganizationModel GetOrganization(int patientId);
    }
}
