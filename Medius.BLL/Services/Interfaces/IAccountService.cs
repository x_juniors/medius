﻿using Medius.Common;
using Medius.DAL.DataModels;
using System;
using System.Collections.Generic;

namespace Medius.BLL.Services
{
    public interface IAccountService : IService<PublicAccountModel>
    {
        AccountModel Register(AccountModel account);

        void ChangePassword(PasswordModel password);

        AccountModel GetByTokenValue(Guid tokenValue);

        TokenModel AddToken(TokenModel token, string login, string password);

        TokenModel ClearAllExcept(int accountId, Guid tokenValue);

        IEnumerable<TokenModel> GetAllTokens(int accountId);

        TokenModel RemoveToken(Guid tokenValue);

        AccountModel GetByToken(TokenModel token);

        IEnumerable<RoleModel> GetRoles(AccountModel account);
    }
}
