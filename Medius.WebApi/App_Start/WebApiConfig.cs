﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Routing;

namespace Medius.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            // Default (first only)
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // ICD-10
            config.Routes.MapHttpRoute(
                name: "Api.IcdClasses.Default",
                routeTemplate: "api/icdclasses/{icdclassId}/{controller}"
            );
            config.Routes.MapHttpRoute(
                name: "Api.Blocks.Default",
                routeTemplate: "api/blocks/{blockId}/{controller}"
            );

            // Geo
            config.Routes.MapHttpRoute(
                name: "Api.Regions.Localities",
                routeTemplate: "api/regions/{regionId}/localities",
                defaults: new { controller = "localities" }
            );

            config.Routes.MapHttpRoute(
                name: "Api.Localities.Streets",
                routeTemplate: "api/localities/{localityId}/streets",
                defaults: new { controller = "streets" }
            );

            // Job
            config.Routes.MapHttpRoute(
                name: "Api.Organizations.Jobs",
                routeTemplate: "api/organizations/{organizationId}/jobs",
                defaults: new { controller = "jobs" }
            );

            // Account
            config.Routes.MapHttpRoute(
                name: "Api.Accounts.Tokens",
                routeTemplate: "api/accounts/{accountId}/tokens",
                defaults: new { controller = "tokens" }
            );

            // Main
            config.Routes.MapHttpRoute(
                name: "Api.Patients.Jobs",
                routeTemplate: "api/patients/{id}/jobs/{jobId}",
                defaults: new { controller = "patients" },
                constraints: new { method = new HttpMethodConstraint(HttpMethod.Put) }
            );

            config.Routes.MapHttpRoute(
                name: "Api.Patients.Categories",
                routeTemplate: "api/patients/{id}/categories/{categoryId}",
                defaults: new { controller = "patients" },
                constraints: new { method = new HttpMethodConstraint(HttpMethod.Put) }
            );

            config.Routes.MapHttpRoute(
                name: "Api.Locations.Streets",
                routeTemplate: "api/locations/{id}/streets/{streetId}",
                defaults: new { controller = "locations" },
                constraints: new { method = new HttpMethodConstraint(HttpMethod.Put) }
            );

            config.Routes.MapHttpRoute(
                name: "Api.Patients.Default",
                routeTemplate: "api/patients/{patientId}/{controller}",
                defaults: new { id = RouteParameter.Optional },
                constraints: new { controller = "^locations$|^medcards$" }
            );

            config.Routes.MapHttpRoute(
                name: "Api.Medcards.Hospitalization",
                routeTemplate: "api/medcards/{medcardId}/hospitalization",
                defaults: new { controller = "hospitalizations" }
            );

            config.Routes.MapHttpRoute(
                name: "Api.Medcards.Contract",
                routeTemplate: "api/medcards/{medcardId}/contract",
                defaults: new { controller = "contracts" }
            );

            config.Routes.MapHttpRoute(
                name: "Api.Medcards.FoodMode",
                routeTemplate: "api/medcards/{medcardId}/foodmode",
                defaults: new { controller = "foodmodes" }
            );

            config.Routes.MapHttpRoute(
                name: "Api.Medcards.Treatment",
                routeTemplate: "api/medcards/{medcardId}/treatment",
                defaults: new { controller = "treatments" }
            );

            config.Routes.MapHttpRoute(
                name: "Api.Medcards.TreatmentDiagnose",
                routeTemplate: "api/medcards/{medcardId}/treatmentdiagnose",
                defaults: new { controller = "treatmentdiagnoses" }
            );

            config.Routes.MapHttpRoute(
                name: "Api.Medcards.AdmissionDepartment",
                routeTemplate: "api/medcards/{medcardId}/admissiondepartment",
                defaults: new { controller = "admissiondepartment" }
            );

            config.Routes.MapHttpRoute(
                name: "Api.Medcards.ExpertInspections",
                routeTemplate: "api/medcards/{medcardId}/expertinspections",
                defaults: new { controller = "expertinspections" }
            );

            config.Routes.MapHttpRoute(
                name: "Api.Medcards.EfficiencyMarks",
                routeTemplate: "api/medcards/{medcardId}/efficiencymarks",
                defaults: new { controller = "efficiencymarks" }
            );

            config.Routes.MapHttpRoute(
                name: "Api.Administration.ExaminationType",
                routeTemplate: "api/ExaminationTypes",
                defaults: new { controller = "examinationtypes" }
            );

            config.Routes.MapHttpRoute(
                name: "Api.Administration.Food",
                routeTemplate: "api/Foods",
                defaults: new { controller = "foods" }
            );

            config.Routes.MapHttpRoute(
                name: "Api.Administration.Contract",
                routeTemplate: "api/Contracts",
                defaults: new { controller = "contracts" }
            );

            config.Routes.MapHttpRoute(
                name: "Api.Medcards.LocalTreatment",
                routeTemplate: "api/medcards/{medcardId}/localtreatment",
                defaults: new { controller = "localtreatments" }
            );

            config.Routes.MapHttpRoute(
               name: "Api.Medcards.Notebooks",
               routeTemplate: "api/medcards/{medcardId}/notebooks",
               defaults: new { controller = "notebooks" }
           );

            config.Routes.MapHttpRoute(
               name: "Api.Medcards.ExaminationResults",
               routeTemplate: "api/medcards/{medcardId}/examinationresults",
               defaults: new { controller = "examinationresults" }
           );

            config.Routes.MapHttpRoute(
                name: "Api.Medcards.Default",
                routeTemplate: "api/medcards/{medcardId}/{controller}",
                defaults: new { },
                constraints: new { controller = "operations" } // ^operations$|^expertinspections$|^efficiencymarks$
            );

            // Temperature list
            config.Routes.MapHttpRoute(
               name: "Api.Medcards.Temperatures",
               routeTemplate: "api/medcards/{medcardId}/temperatures",
               defaults: new { controller = "temperatures" }
           );

            config.Routes.MapHttpRoute(
               name: "Api.Medcards.Pulses",
               routeTemplate: "api/medcards/{medcardId}/pulses",
               defaults: new { controller = "pulses" }
           );

            config.Routes.MapHttpRoute(
               name: "Api.Medcards.Pressures",
               routeTemplate: "api/medcards/{medcardId}/pressures",
               defaults: new { controller = "pressures" }
           );
        }
    }
}
