﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Medius.DAL.EDM;
using Microsoft.Ajax.Utilities;
using Novacode;

namespace Medius.WebApi.Services
{
    public class MedcardBuilder : Builder
    {
        private DocX _template;
        private Medcard _medcard;

        public MedcardBuilder(DocX template, Medcard medcard)
        {
            _template = template;
            _medcard = medcard;
        }

        public override void BuildPatient()
        {
            if (_medcard.Patient != null)
            {
                var patient = _medcard.Patient;
                Replace("${p.name}", patient.name != null ? patient.name : "", _template);
                Replace("${p.lastname}", patient.lastname != null ? patient.lastname : "", _template);
                Replace("${p.patronymic}", patient.patronymic != null ? patient.patronymic : "",
                    _template);
                if (patient.rhFactor != null)
                    Replace("${p.rh}", patient.rhFactor == 0 ? "-" : "+", _template);
                else
                    Replace("${p.rh}", "", _template);
                Replace("${p.bt}", patient.bloodType != null ? patient.bloodType.ToString() : "",
                    _template);
                Replace("${s}", patient.sex != null ? patient.sex.ToString() : "", _template);
                if (patient.born != null)
                {
                    var born = (DateTime)patient.born;
                    Replace("${p.age}", (DateTime.Now.Year - born.Year).ToString(), _template);
                    Replace("${p.born}", patient.born.ToString().Substring(0, 10), _template);
                }
                else
                {
                    Replace("${p.age}", "", _template);
                    Replace("${p.born}", "", _template);
                }
                Replace("${p.sensitivity}", patient.sensitivity != null ? patient.sensitivity : "",
                    _template);
            }
            else
            {
                Replace("${p.name}", "", _template);
                Replace("${p.lastname}", "", _template);
                Replace("${p.patronymic}", "", _template);
                Replace("${p.rh}", "", _template);
                Replace("${p.bt}", "", _template);
                Replace("${s}", "", _template);
                Replace("${p.age}", "", _template);
                Replace("${p.born}", "", _template);
                Replace("${p.sensitivity}", "", _template);
            }
        }

        public override void BuildOperations()
        {
            if (_medcard.Operations != null)
            {
                var operations = _medcard.Operations.OrderByDescending(i => i.date);
                var counter = 0;
                foreach (var operation in operations)
                {
                    if (counter <= 2)
                    {
                        Replace("${o.num_" + counter + "}", operation.number != null ? operation.number.ToString() : "", _template);
                        Replace("${o.title_" + counter + "}", operation.title != null ? operation.title : "", _template);
                        Replace("${o.date_" + counter + "}", operation.date != null ? operation.date.ToString().Substring(0, 10) : "", _template);
                        Replace("${o.anm_" + counter + "}", operation.anesthesiaMethod != null ? operation.anesthesiaMethod : "", _template);
                        Replace("${o.comp_" + counter + "}", operation.complications != null ? operation.complications : "", _template);
                        Replace("${o.surgeon_" + counter + "}", operation.surgeon != null ? operation.surgeon : "", _template);
                        Replace("${o.anest_" + counter + "}", operation.anesthetist != null ? operation.anesthetist : "", _template);
                    }
                    counter++;
                }
                for (int i = 3; counter < i; i--)
                {
                    Replace("${o.num_" + (i - 1) + "}", "", _template);
                    Replace("${o.title_" + (i - 1) + "}", "", _template);
                    Replace("${o.date_" + (i - 1) + "}", "", _template);
                    Replace("${o.anm_" + (i - 1) + "}", "", _template);
                    Replace("${o.comp_" + (i - 1) + "}", "", _template);
                    Replace("${o.surgeon_" + (i - 1) + "}", "", _template);
                    Replace("${o.anest_" + (i - 1) + "}", "", _template);
                }
            }
        }

        public override void BuildMedcard()
        {
            Replace("${number}", _medcard.number != null ? _medcard.number : "", _template);
            Replace("${p.rw}", _medcard.rw != null ? _medcard.rw.ToString().Substring(0, 10) : "", _template);
            Replace("${p.hiv}", _medcard.hivInfection != null
                ? _medcard.hivInfection.ToString().Substring(0, 10)
                : "", _template);
            Replace("${hospd}", _medcard.hospitalizationDate != null
                ? _medcard.hospitalizationDate.ToString().Substring(0, 10)
                : "", _template);
            Replace("${separation}", _medcard.Separation != null ? _medcard.Separation.title : "", _template);
            Replace("${cham}", _medcard.chamber != null ? _medcard.chamber.ToString() : "", _template);
            Replace("${sender}", _medcard.sender != null ? _medcard.sender : "", _template);
            Replace("${epic}", "", _template);
        }

        public override void BuildHospitalization()
        {
            if (_medcard.Hospitalization != null)
            {
                var hospitalization = _medcard.Hospitalization;
                Replace("${h.idia}", hospitalization.institutionDiagnosis != null ? hospitalization.institutionDiagnosis : "", _template);
                Replace("${h.hdia}", hospitalization.hospitalizationDiagnosis != null ? hospitalization.hospitalizationDiagnosis : "", _template);
                Replace("${h.cdia}", hospitalization.clinicalDiagnosis != null ? hospitalization.clinicalDiagnosis : "", _template);
                Replace("${h.setd}", hospitalization.settingDate != null ? hospitalization.settingDate.ToString().Substring(0, 10) : "", _template);
                Replace("${h.ah}", hospitalization.afterHours != null ? hospitalization.afterHours.ToString() : "", _template);
                Replace("${h.ht}", hospitalization.hospitalizationPriority != null ? hospitalization.hospitalizationPriority.ToString() : "", _template);
            }
            else
            {
                Replace("${h.idia}", "", _template);
                Replace("${h.hdia}", "", _template);
                Replace("${h.cdia}", "", _template);
                Replace("${h.setd}", "", _template);
                Replace("${h.ah}", "", _template);
            }

        }

        public override void BuildEfficiencyMarks()
        {
            if (_medcard.EfficiencyMarks != null)
            {
                var efficiencyMarks = _medcard.EfficiencyMarks.OrderByDescending(i => i.begin);
                var counter = 0;
                foreach (var efficiencyMark in efficiencyMarks)
                {
                    if (counter <= 3)
                    {
                        Replace("${em.n_" + counter + "}", efficiencyMark.number != null ? efficiencyMark.number : "", _template);
                        Replace("${em.b_" + counter + "}", efficiencyMark.begin != null ? efficiencyMark.begin.ToString().Substring(0, 10) : "", _template);
                        Replace("${em.e_" + counter + "}", efficiencyMark.end != null ? efficiencyMark.end.ToString().Substring(0, 10) : "", _template);
                    }
                    counter++;
                }
                for (int i = 4; counter < i; i--)
                {
                    Replace("${em.n_" + (i - 1) + "}", "", _template);
                    Replace("${em.b_" + (i - 1) + "}", "", _template);
                    Replace("${em.e_" + (i - 1) + "}", "", _template);
                }
            }
        }

        public override void BuildLocations()
        {
            if (_medcard.Patient.Locations != null)
            {
                var locations = _medcard.Patient.Locations;
                var address = new StringBuilder();
                foreach (var location in locations)
                {
                    address.Append(location.note + ":");
                    address.Append(location.Street.Locality.Region.title + ", ");
                    address.Append(location.Street.title + " ");
                    address.Append(location.apartment + ", ");
                }
                Replace("${l.address}", address.ToString().Substring(0, address.Length - 2), _template);
                if (_medcard.Patient.Locations.First().Street.Locality.type != null)
                    Replace("${l.lt}", _medcard.Patient.Locations.First().Street.Locality.type == 1 ? "1" : "2", _template);
                else
                    Replace("${l.lt}", "", _template);
            }
            else
            {
                Replace("${l.address}", "", _template);
                Replace("${l.lt}", "", _template);
            }

        }

        public override void BuildJob()
        {
            if (_medcard.Patient.Job != null)
            {
                var job = _medcard.Patient.Job;
                var workplace = new StringBuilder();
                workplace.Append(job.post != null
                    ? job.post
                    : "");
                if (job.Organization != null)
                {
                    var organization = job.Organization;
                    workplace.Append(organization.title != null
                        ? organization.title
                        : "");
                    workplace.Append(organization.address != null
                        ? organization.address
                        : "");
                }


                Replace("${p.workplace}", workplace.ToString(), _template);
            }
            else
            {
                Replace("${p.workplace}", "", _template);
            }
        }

        public override void BuildTreatmentDiagnose()
        {
            if (_medcard.TreatmentDiagnose != null)
            {
                var treatmentDiagnose = _medcard.TreatmentDiagnose;
                Replace("${d.final}", treatmentDiagnose.final != null ? treatmentDiagnose.final : "", _template);
                Replace("${d.main} ", treatmentDiagnose.main != null ? treatmentDiagnose.main : "", _template);
                Replace("${d.complication}",
                    treatmentDiagnose.complication != null ? treatmentDiagnose.complication : "", _template);
                Replace("${d.related}", treatmentDiagnose.related != null ? treatmentDiagnose.related : "", _template);
            }
            else
            {
                Replace("${d.final}", "", _template);
                Replace("${d.main} ", "", _template);
                Replace("${d.complication}", "", _template);
                Replace("${d.related}", "", _template);
            }
        }

        public override void BuildTreatment()
        {
            if (_medcard.Treatment != null)
            {
                var treatment = _medcard.Treatment;
                Replace("${t.oo}", treatment.oncologicalObserving != null
                    ? treatment.oncologicalObserving.ToString().Substring(0, 10)
                    : "", _template);
                Replace("${t.xo}", treatment.xrayObserving != null
                    ? treatment.xrayObserving.ToString().Substring(0, 10)
                    : "", _template);
                Replace("${t.ot}", treatment.otherTreatment != null
                    ? treatment.otherTreatment : "", _template);
                Replace("${tt}", treatment.otherTreatmentTypes != null
                    ? treatment.otherTreatmentTypes.ToString() : "", _template);
                Replace("${t.re}", treatment.restoredEfficiency != null
                    ? treatment.restoredEfficiency.ToString() : "", _template);
                Replace("${rs}", treatment.result != null
                    ? treatment.result.ToString() : "", _template);
            }
            else
            {
                Replace("${t.oo}", "", _template);
                Replace("${t.xo}", "", _template);
            }
        }

        public override void BuildAddmissionDepartment()
        {
            if (_medcard.AdmissionDepartment != null)
            {
                var admissionDepartment = _medcard.AdmissionDepartment;
                Replace("${ad.compl}", admissionDepartment.complaint != null
                    ? admissionDepartment.complaint
                    : "", _template);
                Replace(" ${ad.disa}", admissionDepartment.diseaseAnamnesis != null
                    ? admissionDepartment.diseaseAnamnesis
                    : "", _template);
                Replace("${ad.life}", admissionDepartment.lifeAnamnesis != null
                    ? admissionDepartment.lifeAnamnesis
                    : "", _template);
                Replace("${ad.obj}", admissionDepartment.objectiveState != null
                    ? admissionDepartment.objectiveState
                    : "", _template);
                Replace("${ad.other}", admissionDepartment.other != null
                    ? admissionDepartment.other
                    : "", _template);
            }
            else
            {
                Replace("${ad.compl}", "", _template);
                Replace(" ${ad.disa}", "", _template);
                Replace("${ad.life}", "", _template);
                Replace("${ad.obj}", "", _template);
                Replace("${ad.other}", "", _template);
            }

        }

        public override void BuildExaminationResults()
        {
            if (_medcard.ExaminationResults != null)
            {
                var examinationResults = _medcard.ExaminationResults;
                var counter = 2;
                var table = _template.Tables[3];
                foreach (var examinationResult in examinationResults)
                {
                    table.InsertRow();
                    table.Rows[counter].Cells[0].Paragraphs[0].Append(examinationResult.date.ToString().Substring(0, 10));
                    table.Rows[counter].Cells[1].Paragraphs[0].Append(examinationResult.note);
                    counter++;
                }
            }
        }

        public override void BuildNotebook()
        {
            if (_medcard.Notebooks != null)
            {
                var notebooks = _medcard.Notebooks;
                var counter = 2;
                var table = _template.Tables[4];
                foreach (var notebook in notebooks)
                {
                    table.InsertRow();
                    table.Rows[counter].Cells[0].Paragraphs[0].Append(notebook.date.ToString().Substring(0, 10));
                    table.Rows[counter].Cells[1].Paragraphs[0].Append(notebook.note);
                    counter++;
                }
            }
        }

        public override void BuildExpertInspections()
        {
            if (_medcard.ExpertInspections != null)
            {
                var expertInspections = _medcard.ExpertInspections;
                var counter = 2;
                var table = _template.Tables[5];
                foreach (var expertInspection in expertInspections)
                {
                    table.InsertRow();
                    table.Rows[counter].Cells[0].Paragraphs[0].Append(expertInspection.date.ToString().Substring(0, 10));
                    table.Rows[counter].Cells[1].Paragraphs[0].Append(expertInspection.note);
                    table.Rows[counter].Cells[2].Paragraphs[0].Append(expertInspection.author);
                    counter++;
                }
            }
        }

        public override void BuildContract()
        {
            if (_medcard.Contract != null)
            {
                var contract = _medcard.Contract;
                var localContract = new StringBuilder();
                localContract.Append(contract.title != null ? contract.title + " " : "");
                localContract.Append(contract.description != null ? contract.description : "");
                Replace("${t.ins}", localContract.ToString(), _template);
            }
            else
            {
                Replace("${t.ins}", "", _template);
            }
        }

        public override DocX GetResult()
        {
            return _template;
        }

        private void Replace(string match, string value, DocX document)
        {
            document.ReplaceText(match, value);
        }
    }
}
