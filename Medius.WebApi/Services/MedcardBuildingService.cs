﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Microsoft.Office.Interop.Word;
using Medius.DAL.EDM;
using Novacode;

namespace Medius.WebApi.Services
{
    public class MedcardBuildingService
    {
        private string _templatePath;
        private string _resultPath;

        public MedcardBuildingService(string templatePath, string resultPath)
        {
            _templatePath = templatePath;
            _resultPath = resultPath;
        }

        public string CreateMedcard(Medcard medcard)
        {
            DocX resultDocument = null;
            //var wordApp = new Application { Visible = false };
            try
            {
                DocX template = DocX.Load(_templatePath);
                Director director = new Director();
                Builder builder = new MedcardBuilder(template, medcard);
                director.Construct(builder);
                resultDocument = builder.GetResult();
                resultDocument.SaveAs(_resultPath);
            }
            finally
            {
                if (resultDocument != null)
                {
                    resultDocument.Dispose();
                }
                //wordApp.Quit();
            }
            return _resultPath.ToString();
        }
    }
}
