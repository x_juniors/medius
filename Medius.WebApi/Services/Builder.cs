﻿using Novacode;

namespace Medius.WebApi.Services
{
    public abstract class Builder
    {
        public virtual void BuildPatient() { }
        public virtual void BuildJob() { }
        public virtual void BuildLocations() { }
        public virtual void BuildMedcard() { }
        public virtual void BuildHospitalization() { }
        public virtual void BuildTreatment() { }
        public virtual void BuildOperations() { }
        public virtual void BuildAddmissionDepartment() { }
        public virtual void BuildExaminationResults() { }
        public virtual void BuildNotebook() { }
        public virtual void BuildExpertInspections() { }
        public virtual void BuildTreatmentDiagnose() { }
        public virtual void BuildEfficiencyMarks() { }
        public virtual void BuildContract() { }

        public abstract DocX GetResult();
    }
}
