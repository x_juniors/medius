﻿
namespace Medius.WebApi.Services
{
    public class Director
    {
        public void Construct(Builder builder)
        {
            builder.BuildPatient();
            builder.BuildJob();
            builder.BuildLocations();
            builder.BuildMedcard();
            builder.BuildHospitalization();
            builder.BuildTreatment();
            builder.BuildOperations();
            builder.BuildAddmissionDepartment();
            builder.BuildExaminationResults();
            builder.BuildNotebook();
            builder.BuildExpertInspections();
            builder.BuildTreatmentDiagnose();
            builder.BuildEfficiencyMarks();
            builder.BuildContract();
        }
    }
}
