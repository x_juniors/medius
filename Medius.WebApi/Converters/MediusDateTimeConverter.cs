﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Globalization;

namespace Medius.WebApi.Converters
{
    public class MediusDateTimeConverter : DateTimeConverterBase
    {
        public const string DATETIME_FORMAT = "yyyyMMddTHHmmss";

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            DateTime dt;
            if (reader.Value != null)
            {
                DateTime.TryParseExact(reader.Value.ToString(), MediusDateTimeConverter.DATETIME_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
                return dt;
            }
            return null;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((DateTime)value).ToString(MediusDateTimeConverter.DATETIME_FORMAT));
        }
    }
}