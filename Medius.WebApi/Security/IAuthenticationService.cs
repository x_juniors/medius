﻿using Medius.DAL.DataModels;
using System;

namespace Medius.WebApi.Security
{
    public interface IAuthenticationService
    {
        MediusPrincipal AuthenticateByToken(Guid tokenValue);
    }
}
