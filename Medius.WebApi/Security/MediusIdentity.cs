﻿using Medius.DAL.DataModels;
using System.Security.Principal;

namespace Medius.WebApi.Security
{
    public class MediusIdentity : IIdentity
    {
        public AccountModel Account { get; private set; }

        public string AuthenticationType
        {
            get { return "Custom"; }
        }

        public bool IsAuthenticated
        {
            get { return Account != null; }
        }

        public string Name { get { return Account.id.ToString(); } }

        public MediusIdentity(AccountModel account)
        {
            Account = account;
        }
    }
}