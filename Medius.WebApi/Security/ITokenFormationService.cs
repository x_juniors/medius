﻿using Medius.DAL.DataModels;
using System;
using System.Net.Http;

namespace Medius.WebApi.Security
{
    public interface ITokenFormationService
    {
        TokenModel ExtractToken(HttpRequestMessage request);

        Guid ExtractTokenValue(HttpRequestMessage request);

        void FillToken(HttpRequestMessage request, TokenModel token);
    }
}