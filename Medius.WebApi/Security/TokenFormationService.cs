﻿using Medius.DAL.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace Medius.WebApi.Security
{
    public class TokenFormationService : ITokenFormationService
    {
        private const string TOKEN_HEADER = @"Token";

        public TokenModel ExtractToken(HttpRequestMessage request)
        {
            var tokenValue = this.ExtractTokenValue(request);
            var token = new TokenModel { value = tokenValue };
            this.FillToken(request, token);
            return token;
        }

        public Guid ExtractTokenValue(HttpRequestMessage request)
        {
            Guid tokenValue = Guid.Empty;
            IEnumerable<string> values;

            if (request.Headers.TryGetValues(TokenFormationService.TOKEN_HEADER, out values))
            {
                if (values.Count() > 0)
                {
                    var tokenString = values.ToArray()[0];
                    Guid.TryParse(tokenString, out tokenValue);
                }
            }
            return tokenValue;
        }

        public void FillToken(HttpRequestMessage request, TokenModel token)
        {
            var contextWrapper = ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request;
            token.ipAddress = contextWrapper.UserHostAddress;
            token.userAgent = contextWrapper.Browser.Browser;
        }
    }
}