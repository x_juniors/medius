﻿using Medius.BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Medius.WebApi.Security
{
    public class MediusAuthorizeAttribute : AuthorizeAttribute
    {
        private IAuthenticationService _authenticationService;
        private ITokenFormationService _tokenFormationService;

        public MediusAuthorizeAttribute(IAuthenticationService authenticationService, ITokenFormationService tokenFormationService)
        {
            _authenticationService = authenticationService;
            _tokenFormationService = tokenFormationService;
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (SkipAuthorization(actionContext))
            {
                return;
            }
            var tokenValue = _tokenFormationService.ExtractTokenValue(actionContext.Request);
            var principal = _authenticationService.AuthenticateByToken(tokenValue);
            if (principal == null)
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }
            else
            {
                Thread.CurrentPrincipal = principal;
                if (HttpContext.Current != null)
                {
                    HttpContext.Current.User = principal;
                }
            }
        }

        private static bool SkipAuthorization(HttpActionContext actionContext)
        {
            return actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any()
                   || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();
        }
    }
}