﻿using Medius.BLL.Services;
using System;
using System.Linq;

namespace Medius.WebApi.Security
{
    public class AuthenticationService : IAuthenticationService
    {
        private IAccountService _accountService;

        public AuthenticationService(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public MediusPrincipal AuthenticateByToken(Guid tokenValue)
        {
            var account = _accountService.GetByTokenValue(tokenValue);
            if (account == null)
            {
                return null;
            }
            var identity = new MediusIdentity(account);
            var roles = _accountService.GetRoles(account).Select(p => p.roleTitle).ToArray();
            var principal = new MediusPrincipal(identity, roles);
            return principal;
        }
    }
}