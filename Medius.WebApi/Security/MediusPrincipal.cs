﻿using Medius.BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace Medius.WebApi.Security
{
    public class MediusPrincipal : IPrincipal
    {
        private string[] _roles;
        private MediusIdentity _identity;
        public IIdentity Identity
        {
            get { return _identity; }
        }

        public bool IsInRole(string role)
        {
            return _roles.Contains(role);
        }

        public MediusPrincipal(MediusIdentity identity, string[] roles)
        {
            _identity = identity;
            _roles = roles;
        }
    }
}