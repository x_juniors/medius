﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class JobsController : ApiController
    {
        private MediusContext db = new MediusContext();

        public JobsController()
        {
            this.db.Configuration.LazyLoadingEnabled = false;
        }

        // GET: api/Organizations/5/Jobs?expression=str...
        public IQueryable<Job> GetStreets(int organizationId, [FromUri] string expression, int limit = 16)
        {
            var query = from s in db.Jobs.Where(p => p.organizationId == organizationId)
                        where s.post.ToLower().Contains(expression.ToLower())
                        select s;
            return query.Take(limit);
        }

        // GET: api/Jobs
        public IQueryable<Job> GetJobs(int organizationId)
        {
            return db.Jobs.Where(p => p.organizationId == organizationId);
        }

        // GET: api/Patient/5/Jobs
        //public Job GetPatientJob(int patientId)
        //{
        //    var job = db.Jobs.Find( db.Patients.Find(patientId).jobId);
        //    job.Patients.Clear();
        //    return job;
        //}

        // GET: api/Jobs/5
        [ResponseType(typeof(Job))]
        public IHttpActionResult GetJob(int id)
        {
            Job job = db.Jobs.Find(id);
            if (job == null)
            {
                return NotFound();
            }

            return Ok(job);
        }

        // PUT: api/Jobs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutJob(int id, Job job)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != job.id)
            {
                return BadRequest();
            }

            db.Entry(job).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JobExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Jobs
        [ResponseType(typeof(Job))]
        public IHttpActionResult PostJob(int organizationId, Job job)
        {
            job.organizationId = organizationId;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Jobs.Add(job);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (JobExists(job.id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = job.id }, job);
        }

        // DELETE: api/Jobs/5
        [ResponseType(typeof(Job))]
        public IHttpActionResult DeleteJob(int id)
        {
            Job job = db.Jobs.Find(id);
            if (job == null)
            {
                return NotFound();
            }

            db.Jobs.Remove(job);
            db.SaveChanges();

            return Ok(job);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool JobExists(int id)
        {
            return db.Jobs.Count(e => e.id == id) > 0;
        }
    }
}