﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class LocalTreatmentsController : ApiController
    {
        private MediusContext db = new MediusContext();

        // GET: api/LocalTreatments
        public IQueryable<LocalTreatment> GetLocalTreatments()
        {
            return db.LocalTreatments;
        }

        // GET: api/LocalTreatments/5
        [ResponseType(typeof(LocalTreatment))]
        public IHttpActionResult GetLocalTreatment(int id)
        {
            LocalTreatment localTreatment = db.LocalTreatments.Find(id);
            if (localTreatment == null)
            {
                return NotFound();
            }

            return Ok(localTreatment);
        }

        // PUT: api/LocalTreatments/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLocalTreatment(int id, LocalTreatment localTreatment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != localTreatment.id)
            {
                return BadRequest();
            }

            db.Entry(localTreatment).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LocalTreatmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/LocalTreatments
        [ResponseType(typeof(LocalTreatment))]
        public IHttpActionResult PostLocalTreatment(LocalTreatment localTreatment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.LocalTreatments.Add(localTreatment);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = localTreatment.id }, localTreatment);
        }

        // DELETE: api/LocalTreatments/5
        [ResponseType(typeof(LocalTreatment))]
        public IHttpActionResult DeleteLocalTreatment(int id)
        {
            LocalTreatment localTreatment = db.LocalTreatments.Find(id);
            if (localTreatment == null)
            {
                return NotFound();
            }

            db.LocalTreatments.Remove(localTreatment);
            db.SaveChanges();

            return Ok(localTreatment);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LocalTreatmentExists(int id)
        {
            return db.LocalTreatments.Count(e => e.id == id) > 0;
        }
    }
}