﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class HospitalizationsController : ApiController
    {
        private MediusContext db = new MediusContext();

        public HospitalizationsController()
        {
            db.Configuration.LazyLoadingEnabled = false;
        }

        // GET: api/Medcards/5/Hospitalizations
        [ResponseType(typeof(Hospitalization))]
        public IHttpActionResult GetHospitalization(int medcardId)
        {
            Hospitalization hospitalization = db.Hospitalizations.Find(medcardId);
            if (hospitalization == null)
            {
                return NotFound();
            }

            return Ok(hospitalization);
        }

        // GET: api/Hospitalizations/5
        //[ResponseType(typeof(Hospitalization))]
        //public IHttpActionResult GetHospitalization(int id)
        //{
        //    Hospitalization hospitalization = db.Hospitalizations.Find(id);
        //    if (hospitalization == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(hospitalization);
        //}

        // PUT: api/Hospitalizations/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutHospitalization(int id, Hospitalization hospitalization)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != hospitalization.id)
            {
                return BadRequest();
            }

            db.Entry(hospitalization).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HospitalizationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Hospitalizations
        [ResponseType(typeof(Hospitalization))]
        public IHttpActionResult PostHospitalization(Hospitalization hospitalization)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Hospitalizations.Add(hospitalization);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = hospitalization.id }, hospitalization);
        }

        // DELETE: api/Hospitalizations/5
        [ResponseType(typeof(Hospitalization))]
        public IHttpActionResult DeleteHospitalization(int id)
        {
            Hospitalization hospitalization = db.Hospitalizations.Find(id);
            if (hospitalization == null)
            {
                return NotFound();
            }

            db.Hospitalizations.Remove(hospitalization);
            db.SaveChanges();

            return Ok(hospitalization);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HospitalizationExists(int id)
        {
            return db.Hospitalizations.Count(e => e.id == id) > 0;
        }
    }
}