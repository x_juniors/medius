﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class LocationsController : ApiController
    {
        private MediusContext db = new MediusContext();

        public LocationsController()
        {
            this.db.Configuration.LazyLoadingEnabled = false;
        }

        // GET: api/Locations
        public IQueryable<Location> GetLocations(int patientId)
        {
            return db.Locations.Where(p => p.patientId == patientId);
        }

        // GET: api/Locations/5/street
        [Route("api/locations/{id}/street")]
        [ResponseType(typeof(Street))]
        public IHttpActionResult GetStreet(int id)
        {
            Location location = db.Locations.Find(id);
            if (location == null)
            {
                return NotFound();
            }
            db.Entry(location).Reference(e => e.Street).Load();
            var street = location.Street;
            if (street == null)
            {
                return NotFound();
            }
            street.Locations.Clear();
            return Ok(street);
        }

        // GET: api/Locations/5/street/locality
        [Route("api/locations/{id}/street/locality")]
        [ResponseType(typeof(Locality))]
        public IHttpActionResult GetLocality(int id)
        {
            Location location = db.Locations.Find(id);
            if (location == null)
            {
                return NotFound();
            }
            db.Entry(location).Reference(e => e.Street).Load();
            if (location.streetId == null)
            {
                return NotFound();
            } 
            db.Entry(location.Street).Reference(e => e.Locality).Load();
            var locality = location.Street.Locality;
            if (locality == null)
            {
                return NotFound();
            }
            locality.Streets.Clear();
            return Ok(locality);
        }

        // GET: api/Locations/5/street/locality/region
        [Route("api/locations/{id}/street/locality/region")]
        [ResponseType(typeof(Region))]
        public IHttpActionResult GetRegion(int id)
        {
            Location location = db.Locations.Find(id);
            if (location == null)
            {
                return NotFound();
            }
            db.Entry(location).Reference(e => e.Street).Load();
            if (location.Street == null)
            {
                return NotFound();
            }
            db.Entry(location.Street).Reference(e => e.Locality).Load();
            if (location.Street.Locality == null)
            {
                return NotFound();
            }
            db.Entry(location.Street.Locality).Reference(e => e.Region).Load();
            var region = location.Street.Locality.Region;
            if (region == null)
            {
                return NotFound();
            }
            region.Localities.Clear();
            return Ok(region);
        }

        // GET: api/Locations/5
        [ResponseType(typeof(Location))]
        public IHttpActionResult GetLocation(int id)
        {
            Location location = db.Locations.Find(id);
            if (location == null)
            {
                return NotFound();
            }

            return Ok(location);
        }

        // PUT: api/Locations/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLocation(int id, Location location)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != location.id)
            {
                return BadRequest();
            }

            db.Entry(location).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LocationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // PUT: api/Locations/5/streets/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutLocation(int id, int? streetId = null)
        //{
        //    var location = db.Locations.Find(id);
        //    location.streetId = streetId;
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != location.id)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(location).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!LocationExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        // POST: api/Locations
        [ResponseType(typeof(Location))]
        public IHttpActionResult PostLocation(Location location)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Locations.Add(location);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (LocationExists(location.id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = location.patientId }, location);
        }

        // DELETE: api/Locations/5
        [ResponseType(typeof(Location))]
        public IHttpActionResult DeleteLocation(int id)
        {
            Location location = db.Locations.Find(id);
            if (location == null)
            {
                return NotFound();
            }

            db.Locations.Remove(location);
            db.SaveChanges();

            return Ok(location);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LocationExists(int id)
        {
            return db.Locations.Count(e => e.id == id) > 0;
        }
    }
}