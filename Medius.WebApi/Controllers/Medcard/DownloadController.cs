﻿using Medius.DAL.EDM;
using Medius.WebApi.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class DownloadController : ApiController
    {
        const string MSWORD_CONTENT_TYPE = @"application/msword";

        // GET: api/Download/5
        [Route("api/medcards/{medcardId}/download")]
        public HttpResponseMessage Get(int medcardId)
        {
            string appDataDir = AppDomain.CurrentDomain.BaseDirectory + @"App_Data\\";
            string templateName = @"template_medcard.docx";
            string resultName = @"medcard_xxx.docx";
            string templatePath = appDataDir + templateName;
            string resultPath = appDataDir + resultName;
            using (MediusContext context = new MediusContext())
            {
                var medcard = context.Medcards.Find(medcardId);
                if (medcard == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }
                var medcardBuildingService = new MedcardBuildingService(templatePath, resultPath);
                medcardBuildingService.CreateMedcard(medcard);
            }
            
            byte[] content = null;
            using (FileStream stream = File.Open(resultPath, FileMode.Open))
            {
                content = new byte[stream.Length];
                stream.Read(content, 0, (int)stream.Length);
            }
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(content);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(DownloadController.MSWORD_CONTENT_TYPE);
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "medcard_" + medcardId.ToString() + ".docx"
            };
            return response;
        }
    }
}
