﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class NotebooksController : ApiController
    {
        private MediusContext db = new MediusContext();

        public NotebooksController()
        {
            db.Configuration.LazyLoadingEnabled = false;
        }

        // GET: api/Notebooks
        public IQueryable<Notebook> GetNotebooks(int medcardId)
        {
            return db.Notebooks.Where(x=>x.medcardId == medcardId);
        }

        // GET: api/Notebooks/5
        [ResponseType(typeof(Notebook))]
        public IHttpActionResult GetNotebook(int id)
        {
            Notebook notebook = db.Notebooks.Find(id);
            if (notebook == null)
            {
                return NotFound();
            }

            return Ok(notebook);
        }

        // PUT: api/Notebooks/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutNotebook(int id, Notebook notebook)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != notebook.id)
            {
                return BadRequest();
            }

            db.Entry(notebook).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NotebookExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Medcards/5/Notebooks
        [ResponseType(typeof(Notebook))]
        public IHttpActionResult PostNotebook(int medcardId, Notebook notebook)
        {
            notebook.medcardId = medcardId;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Notebooks.Add(notebook);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = notebook.id }, notebook);
        }

        // DELETE: api/Notebooks/5
        [ResponseType(typeof(Notebook))]
        public IHttpActionResult DeleteNotebook(int id)
        {
            Notebook notebook = db.Notebooks.Find(id);
            if (notebook == null)
            {
                return NotFound();
            }

            db.Notebooks.Remove(notebook);
            db.SaveChanges();

            return Ok(notebook);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NotebookExists(int id)
        {
            return db.Notebooks.Count(e => e.id == id) > 0;
        }
    }
}