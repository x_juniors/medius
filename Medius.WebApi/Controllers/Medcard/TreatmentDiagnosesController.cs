﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class TreatmentDiagnosesController : ApiController
    {
        private MediusContext db = new MediusContext();

        public TreatmentDiagnosesController()
        {
            db.Configuration.LazyLoadingEnabled = false;
        }

        // GET: api/TreatmentDiagnoses
        public IQueryable<TreatmentDiagnose> GetTreatmentDiagnoses()
        {
            return db.TreatmentDiagnoses;
        }

        // GET: api/TreatmentDiagnoses/5
        [ResponseType(typeof(TreatmentDiagnose))]
        public IHttpActionResult GetTreatmentDiagnose(int medcardId)
        {
            TreatmentDiagnose treatmentDiagnose = db.TreatmentDiagnoses.Find(medcardId);
            if (treatmentDiagnose == null)
            {
                return NotFound();
            }

            return Ok(treatmentDiagnose);
        }

        // PUT: api/TreatmentDiagnoses/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTreatmentDiagnose(int id, TreatmentDiagnose treatmentDiagnose)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != treatmentDiagnose.id)
            {
                return BadRequest();
            }

            db.Entry(treatmentDiagnose).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TreatmentDiagnoseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TreatmentDiagnoses
        [ResponseType(typeof(TreatmentDiagnose))]
        public IHttpActionResult PostTreatmentDiagnose(TreatmentDiagnose treatmentDiagnose)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TreatmentDiagnoses.Add(treatmentDiagnose);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = treatmentDiagnose.id }, treatmentDiagnose);
        }

        // DELETE: api/TreatmentDiagnoses/5
        [ResponseType(typeof(TreatmentDiagnose))]
        public IHttpActionResult DeleteTreatmentDiagnose(int id)
        {
            TreatmentDiagnose treatmentDiagnose = db.TreatmentDiagnoses.Find(id);
            if (treatmentDiagnose == null)
            {
                return NotFound();
            }

            db.TreatmentDiagnoses.Remove(treatmentDiagnose);
            db.SaveChanges();

            return Ok(treatmentDiagnose);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TreatmentDiagnoseExists(int id)
        {
            return db.TreatmentDiagnoses.Count(e => e.id == id) > 0;
        }
    }
}