﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class AdmissionDepartmentsController : ApiController
    {
        private MediusContext db = new MediusContext();

        public AdmissionDepartmentsController()
        {
            db.Configuration.LazyLoadingEnabled = false;
        }
        // GET: api/AdmissionDepartments
        public IQueryable<AdmissionDepartment> GetAdmissionDepartments()
        {
            return db.AdmissionDepartments;
        }

        // GET: api/AdmissionDepartments/5
        [ResponseType(typeof(AdmissionDepartment))]
        public IHttpActionResult GetAdmissionDepartment(int id)
        {
            AdmissionDepartment admissionDepartment = this.db.AdmissionDepartments.Find(id);
            if (admissionDepartment == null)
            {
                return NotFound();
            }
            return Ok(admissionDepartment);
        }

        // PUT: api/AdmissionDepartments/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAdmissionDepartment(int id, AdmissionDepartment admissionDepartment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != admissionDepartment.id)
            {
                return BadRequest();
            }

            db.Entry(admissionDepartment).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AdmissionDepartmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AdmissionDepartments
        [ResponseType(typeof(AdmissionDepartment))]
        public IHttpActionResult PostAdmissionDepartment(int medcardId, AdmissionDepartment admissionDepartment)
        {
            admissionDepartment.id = medcardId;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.AdmissionDepartments.Add(admissionDepartment);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = admissionDepartment.id }, admissionDepartment);
        }

        // DELETE: api/AdmissionDepartments/5
        [ResponseType(typeof(AdmissionDepartment))]
        public IHttpActionResult DeleteAdmissionDepartment(int id)
        {
            AdmissionDepartment admissionDepartment = db.AdmissionDepartments.Find(id);
            if (admissionDepartment == null)
            {
                return NotFound();
            }

            db.AdmissionDepartments.Remove(admissionDepartment);
            db.SaveChanges();

            return Ok(admissionDepartment);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AdmissionDepartmentExists(int id)
        {
            return db.AdmissionDepartments.Count(e => e.id == id) > 0;
        }
    }
}