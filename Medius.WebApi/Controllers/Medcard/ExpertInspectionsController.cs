﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class ExpertInspectionsController : ApiController
    {
        private MediusContext db = new MediusContext();

        public ExpertInspectionsController ()
        {
            db.Configuration.LazyLoadingEnabled = false;
        }

        // GET: api/ExpertInspections
        public IQueryable<ExpertInspection> GetExpertInspections(int medcardId)
        {
            return db.ExpertInspections.Where(i => i.medcardId == medcardId);
        }

        // GET: api/ExpertInspections/5
        [ResponseType(typeof(ExpertInspection))]
        public IHttpActionResult GetExpertInspection(int id)
        {
            ExpertInspection expertInspection = db.ExpertInspections.Find(id);
            if (expertInspection == null)
            {
                return NotFound();
            }

            return Ok(expertInspection);
        }

        // PUT: api/ExpertInspections/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutExpertInspection(int id, ExpertInspection expertInspection)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != expertInspection.id)
            {
                return BadRequest();
            }

            db.Entry(expertInspection).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExpertInspectionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ExpertInspections
        [ResponseType(typeof(ExpertInspection))]
        public IHttpActionResult PostExpertInspection(int medcardId, ExpertInspection expertInspection)
        {
            expertInspection.medcardId = medcardId;
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            db.ExpertInspections.Add(expertInspection);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = expertInspection.id }, expertInspection);
        }

        // DELETE: api/ExpertInspections/5
        [ResponseType(typeof(ExpertInspection))]
        public IHttpActionResult DeleteExpertInspection(int id)
        {
            ExpertInspection expertInspection = db.ExpertInspections.Find(id);
            if (expertInspection == null)
            {
                return NotFound();
            }

            db.ExpertInspections.Remove(expertInspection);
            db.SaveChanges();

            return Ok(expertInspection);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ExpertInspectionExists(int id)
        {
            return db.ExpertInspections.Count(e => e.id == id) > 0;
        }
    }
}