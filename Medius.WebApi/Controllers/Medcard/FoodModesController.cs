﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class FoodModesController : ApiController
    {
        private MediusContext db = new MediusContext();

        // GET: api/Medcards/5/FoodModes
        public IQueryable<FoodMode> GetFoodModes(int medcardId)
        {
            return db.FoodModes.Where(p => p.id == medcardId);
        }

        // GET: api/FoodModes/5
        [ResponseType(typeof(FoodMode))]
        public IHttpActionResult GetFoodMode(int id)
        {
            FoodMode foodMode = db.FoodModes.Find(id);
            if (foodMode == null)
            {
                return NotFound();
            }

            return Ok(foodMode);
        }

        // PUT: api/FoodModes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFoodMode(int id, FoodMode foodMode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != foodMode.id)
            {
                return BadRequest();
            }

            db.Entry(foodMode).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FoodModeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/FoodModes
        [ResponseType(typeof(FoodMode))]
        public IHttpActionResult PostFoodMode(FoodMode foodMode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.FoodModes.Add(foodMode);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (FoodModeExists(foodMode.id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = foodMode.id }, foodMode);
        }

        // DELETE: api/FoodModes/5
        [ResponseType(typeof(FoodMode))]
        public IHttpActionResult DeleteFoodMode(int id)
        {
            FoodMode foodMode = db.FoodModes.Find(id);
            if (foodMode == null)
            {
                return NotFound();
            }

            db.FoodModes.Remove(foodMode);
            db.SaveChanges();

            return Ok(foodMode);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FoodModeExists(int id)
        {
            return db.FoodModes.Count(e => e.id == id) > 0;
        }
    }
}