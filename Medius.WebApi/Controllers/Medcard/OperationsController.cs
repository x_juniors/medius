﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class OperationsController : ApiController
    {
        private MediusContext db = new MediusContext();

        public OperationsController()
        {
            db.Configuration.LazyLoadingEnabled = false;
        }

        // GET: api/Operations
        public IQueryable<Operation> GetOperations()
        {
            return db.Operations;
        }

        public IQueryable<Operation> GetOperations(int medcardId)
        {
            return db.Operations.Where(i => i.medcardId == medcardId);
        }
        // GET: api/Operations/5
        [ResponseType(typeof(Operation))]
        public IHttpActionResult GetOperation(int id)
        {
            Operation operation = db.Operations.Find(id);
            if (operation == null)
            {
                return NotFound();
            }

            return Ok(operation);
        }

        // PUT: api/Operations/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOperation(int id, Operation operation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != operation.id)
            {
                return BadRequest();
            }

            db.Entry(operation).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OperationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Operations
        [ResponseType(typeof(Operation))]
        public IHttpActionResult PostOperation(int medcardId, Operation operation)
        {
            operation.medcardId = medcardId;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Operations.Add(operation);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = operation.id }, operation);
        }

        // DELETE: api/Operations/5
        [ResponseType(typeof(Operation))]
        public IHttpActionResult DeleteOperation(int id)
        {
            Operation operation = db.Operations.Find(id);
            if (operation == null)
            {
                return NotFound();
            }

            db.Operations.Remove(operation);
            db.SaveChanges();

            return Ok(operation);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OperationExists(int id)
        {
            return db.Operations.Count(e => e.id == id) > 0;
        }
    }
}