﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class EfficiencyMarksController : ApiController
    {
        private MediusContext db = new MediusContext();

        public EfficiencyMarksController()
        {
            db.Configuration.LazyLoadingEnabled = false;
        }

        // GET: api/EfficiencyMarks
        public IQueryable<EfficiencyMark> GetEfficiencyMarks(int medcardId)
        {
            return db.EfficiencyMarks.Where(i => i.medcardId == medcardId);
        }

        // GET: api/EfficiencyMarks/5
        [ResponseType(typeof(EfficiencyMark))]
        public IHttpActionResult GetEfficiencyMark(int id)
        {
            EfficiencyMark efficiencyMark = db.EfficiencyMarks.Find(id);
            if (efficiencyMark == null)
            {
                return NotFound();
            }

            return Ok(efficiencyMark);
        }

        // PUT: api/EfficiencyMarks/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEfficiencyMark(int id, EfficiencyMark efficiencyMark)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != efficiencyMark.id)
            {
                return BadRequest();
            }

            db.Entry(efficiencyMark).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EfficiencyMarkExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/EfficiencyMarks
        [ResponseType(typeof(EfficiencyMark))]
        public IHttpActionResult PostEfficiencyMark(int medcardId, EfficiencyMark efficiencyMark)
        {
            efficiencyMark.medcardId = medcardId;
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            db.EfficiencyMarks.Add(efficiencyMark);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = efficiencyMark.id }, efficiencyMark);
        }

        // DELETE: api/EfficiencyMarks/5
        [ResponseType(typeof(EfficiencyMark))]
        public IHttpActionResult DeleteEfficiencyMark(int id)
        {
            EfficiencyMark efficiencyMark = db.EfficiencyMarks.Find(id);
            if (efficiencyMark == null)
            {
                return NotFound();
            }

            db.EfficiencyMarks.Remove(efficiencyMark);
            db.SaveChanges();

            return Ok(efficiencyMark);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EfficiencyMarkExists(int id)
        {
            return db.EfficiencyMarks.Count(e => e.id == id) > 0;
        }
    }
}