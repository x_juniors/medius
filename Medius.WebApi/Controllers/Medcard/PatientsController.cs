﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.DataModels;
using Medius.BLL.Services;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class PatientsController : ApiController
    {
        private IPatientService service;

        public PatientsController(IPatientService service)
        {
            this.service = service;
        }

        // GET: api/patients
        public IEnumerable<PatientModel> GetPatients()
        {
            return this.service.Get();
        }

        // GET: api/patients/5/category
        [Route("api/patients/{id}/category")]
        public IHttpActionResult GetPatientCategory(int id)
        {
            var category = this.service.GetCategory(id);
            if (category == null)
            {
                return NotFound();
            }
            return Ok(category);
        }

        // GET: api/patients/5/job
        [Route("api/patients/{id}/job")]
        public IHttpActionResult GetPatientJob(int id)
        {
            var job = this.service.GetJob(id);
            if (job == null)
            {
                return NotFound();
            }
            return Ok(job);
        }

        // GET: api/patients/5/job/organization
        [Route("api/patients/{id}/job/organization")]
        public IHttpActionResult GetPatientOrganization(int id)
        {
            var organization = this.service.GetOrganization(id);
            if (organization == null)
            {
                return NotFound();
            }
            return Ok(organization);
        }

        // GET: api/patients/5
        [ResponseType(typeof(PatientModel))]
        public IHttpActionResult GetPatient(int id)
        {
            var patient = this.service.Get(id);
            if (patient == null)
            {
                return NotFound();
            }
            return Ok(patient);
        }

        // PUT: api/patients/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPatient(int id, PatientModel patient)
        {
            patient.id = id;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                this.service.Update(patient);
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/patients
        [ResponseType(typeof(PatientModel))]
        public IHttpActionResult PostPatient(PatientModel patient)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            patient = service.Add(patient);
            return CreatedAtRoute("DefaultApi", new { id = patient.id }, patient);
        }

        // DELETE: api/Patients/5
        [ResponseType(typeof(PatientModel))]
        public IHttpActionResult DeletePatient(int id)
        {
            var patient = this.service.Remove(id);
            if (patient == null)
            {
                return NotFound();
            }
            return Ok(patient);
        }

        // PUT: api/patients/5/jobs/4
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPatient(int id, int? jobId)
        {
            var patient = this.service.Get(id);
            if (patient == null)
            {
                return NotFound();
            }
            patient.jobId = jobId;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                this.service.Update(patient);
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}