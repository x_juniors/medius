﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class ExaminationResultsController : ApiController
    {
        private MediusContext db = new MediusContext();

        public ExaminationResultsController()
        {
            db.Configuration.LazyLoadingEnabled = false;
        }

        // GET: api/ExaminationResults
        public IQueryable<ExaminationResult> GetExaminationResults(int medcardId)
        {
            return db.ExaminationResults.Where(x => x.medcardId == medcardId);
        }

        // GET: api/ExaminationResults/5
        [ResponseType(typeof(ExaminationResult))]
        public IHttpActionResult GetExaminationResult(int id)
        {
            ExaminationResult examinationResult = db.ExaminationResults.Find(id);
            if (examinationResult == null)
            {
                return NotFound();
            }

            return Ok(examinationResult);
        }

        // PUT: api/ExaminationResults/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutExaminationResult(int id, ExaminationResult examinationResult)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != examinationResult.id)
            {
                return BadRequest();
            }

            db.Entry(examinationResult).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExaminationResultExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ExaminationResults
        [ResponseType(typeof(ExaminationResult))]
        public IHttpActionResult PostExaminationResult(int medcardId, ExaminationResult examinationResult)
        {
            examinationResult.medcardId = medcardId;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ExaminationResults.Add(examinationResult);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = examinationResult.id }, examinationResult);
        }

        // DELETE: api/ExaminationResults/5
        [ResponseType(typeof(ExaminationResult))]
        public IHttpActionResult DeleteExaminationResult(int id)
        {
            ExaminationResult examinationResult = db.ExaminationResults.Find(id);
            if (examinationResult == null)
            {
                return NotFound();
            }

            db.ExaminationResults.Remove(examinationResult);
            db.SaveChanges();

            return Ok(examinationResult);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ExaminationResultExists(int id)
        {
            return db.ExaminationResults.Count(e => e.id == id) > 0;
        }
    }
}