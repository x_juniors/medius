﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class MedcardsController : ApiController
    {
        private MediusContext db = new MediusContext();

        public MedcardsController()
        {
            this.db.Configuration.LazyLoadingEnabled = false;
        }

        // GET: api/Medcards
        public IQueryable<Medcard> GetMedcards()
        {
            return db.Medcards;
        }

        // GET: api/Patients/5/Medcards
        public IQueryable<Medcard> GetMedcards(int patientId)
        {
            return db.Medcards.Where(p => p.patientId == patientId);
        }

        // GET: api/Medcards/5
        [ResponseType(typeof(Medcard))]
        public IHttpActionResult GetMedcard(int id)
        {
            Medcard medcard = db.Medcards.Find(id);
            if (medcard == null)
            {
                return NotFound();
            }

            return Ok(medcard);
        }

        // PUT: api/Medcards/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMedcard(int id, Medcard medcard)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != medcard.id)
            {
                return BadRequest();
            }

            db.Entry(medcard).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedcardExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Patients/5/Medcards
        [ResponseType(typeof(Medcard))]
        public IHttpActionResult PostMedcard(int patientId, Medcard medcard)
        {
            medcard.patientId = patientId;
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            medcard = db.Medcards.Add(medcard);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = medcard.id }, medcard);
        }

        // DELETE: api/Medcards/5
        [ResponseType(typeof(Medcard))]
        public IHttpActionResult DeleteMedcard(int id)
        {
            Medcard medcard = db.Medcards.Find(id);
            if (medcard == null)
            {
                return NotFound();
            }

            db.Medcards.Remove(medcard);
            db.SaveChanges();

            return Ok(medcard);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MedcardExists(int id)
        {
            return db.Medcards.Count(e => e.id == id) > 0;
        }
    }
}