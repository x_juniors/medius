﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    public class ExaminationTypesController : ApiController
    {
        private MediusContext db = new MediusContext();

        public ExaminationTypesController()
        {
            db.Configuration.LazyLoadingEnabled = false;
        }

        // GET: api/ExaminationTypes
        public IQueryable<ExaminationType> GetExaminationTypes()
        {
            return db.ExaminationTypes;
        }

        // GET: api/ExaminationTypes/5
        [ResponseType(typeof(ExaminationType))]
        public IHttpActionResult GetExaminationType(int id)
        {
            ExaminationType examinationType = db.ExaminationTypes.Find(id);
            if (examinationType == null)
            {
                return NotFound();
            }

            return Ok(examinationType);
        }

        // PUT: api/ExaminationTypes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutExaminationType(int id, ExaminationType examinationType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != examinationType.id)
            {
                return BadRequest();
            }

            db.Entry(examinationType).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExaminationTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ExaminationTypes
        [ResponseType(typeof(ExaminationType))]
        public IHttpActionResult PostExaminationType(ExaminationType examinationType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ExaminationTypes.Add(examinationType);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ExaminationTypeExists(examinationType.id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = examinationType.id }, examinationType);
        }

        // DELETE: api/ExaminationTypes/5
        [ResponseType(typeof(ExaminationType))]
        public IHttpActionResult DeleteExaminationType(int id)
        {
            ExaminationType examinationType = db.ExaminationTypes.Find(id);
            if (examinationType == null)
            {
                return NotFound();
            }

            db.ExaminationTypes.Remove(examinationType);
            db.SaveChanges();

            return Ok(examinationType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ExaminationTypeExists(int id)
        {
            return db.ExaminationTypes.Count(e => e.id == id) > 0;
        }
    }
}