﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    public class PhysiotherapiesController : ApiController
    {
        private MediusContext db = new MediusContext();

        // GET: api/Physiotherapies
        public IQueryable<Physiotherapy> GetPhysiotherapies()
        {
            return db.Physiotherapies;
        }

        // GET: api/Physiotherapies/5
        [ResponseType(typeof(Physiotherapy))]
        public IHttpActionResult GetPhysiotherapy(int id)
        {
            Physiotherapy physiotherapy = db.Physiotherapies.Find(id);
            if (physiotherapy == null)
            {
                return NotFound();
            }

            return Ok(physiotherapy);
        }

        // PUT: api/Physiotherapies/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPhysiotherapy(int id, Physiotherapy physiotherapy)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != physiotherapy.id)
            {
                return BadRequest();
            }

            db.Entry(physiotherapy).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PhysiotherapyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Physiotherapies
        [ResponseType(typeof(Physiotherapy))]
        public IHttpActionResult PostPhysiotherapy(Physiotherapy physiotherapy)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Physiotherapies.Add(physiotherapy);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = physiotherapy.id }, physiotherapy);
        }

        // DELETE: api/Physiotherapies/5
        [ResponseType(typeof(Physiotherapy))]
        public IHttpActionResult DeletePhysiotherapy(int id)
        {
            Physiotherapy physiotherapy = db.Physiotherapies.Find(id);
            if (physiotherapy == null)
            {
                return NotFound();
            }

            db.Physiotherapies.Remove(physiotherapy);
            db.SaveChanges();

            return Ok(physiotherapy);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PhysiotherapyExists(int id)
        {
            return db.Physiotherapies.Count(e => e.id == id) > 0;
        }
    }
}