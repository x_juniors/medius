﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    public class ExaminationsController : ApiController
    {
        private MediusContext db = new MediusContext();

        // GET: api/Examinations
        public IQueryable<Examination> GetExaminations()
        {
            return db.Examinations;
        }

        // GET: api/Examinations/5
        [ResponseType(typeof(Examination))]
        public IHttpActionResult GetExamination(int id)
        {
            Examination examination = db.Examinations.Find(id);
            if (examination == null)
            {
                return NotFound();
            }

            return Ok(examination);
        }

        // PUT: api/Examinations/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutExamination(int id, Examination examination)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != examination.id)
            {
                return BadRequest();
            }

            db.Entry(examination).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExaminationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Examinations
        [ResponseType(typeof(Examination))]
        public IHttpActionResult PostExamination(Examination examination)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Examinations.Add(examination);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = examination.id }, examination);
        }

        // DELETE: api/Examinations/5
        [ResponseType(typeof(Examination))]
        public IHttpActionResult DeleteExamination(int id)
        {
            Examination examination = db.Examinations.Find(id);
            if (examination == null)
            {
                return NotFound();
            }

            db.Examinations.Remove(examination);
            db.SaveChanges();

            return Ok(examination);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ExaminationExists(int id)
        {
            return db.Examinations.Count(e => e.id == id) > 0;
        }
    }
}