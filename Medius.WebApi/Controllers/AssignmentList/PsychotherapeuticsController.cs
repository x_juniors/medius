﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    public class PsychotherapeuticsController : ApiController
    {
        private MediusContext db = new MediusContext();

        // GET: api/Psychotherapeutics
        public IQueryable<Psychotherapeutic> GetPsychotherapeutics()
        {
            return db.Psychotherapeutics;
        }

        // GET: api/Psychotherapeutics/5
        [ResponseType(typeof(Psychotherapeutic))]
        public IHttpActionResult GetPsychotherapeutic(int id)
        {
            Psychotherapeutic psychotherapeutic = db.Psychotherapeutics.Find(id);
            if (psychotherapeutic == null)
            {
                return NotFound();
            }

            return Ok(psychotherapeutic);
        }

        // PUT: api/Psychotherapeutics/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPsychotherapeutic(int id, Psychotherapeutic psychotherapeutic)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != psychotherapeutic.id)
            {
                return BadRequest();
            }

            db.Entry(psychotherapeutic).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PsychotherapeuticExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Psychotherapeutics
        [ResponseType(typeof(Psychotherapeutic))]
        public IHttpActionResult PostPsychotherapeutic(Psychotherapeutic psychotherapeutic)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Psychotherapeutics.Add(psychotherapeutic);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = psychotherapeutic.id }, psychotherapeutic);
        }

        // DELETE: api/Psychotherapeutics/5
        [ResponseType(typeof(Psychotherapeutic))]
        public IHttpActionResult DeletePsychotherapeutic(int id)
        {
            Psychotherapeutic psychotherapeutic = db.Psychotherapeutics.Find(id);
            if (psychotherapeutic == null)
            {
                return NotFound();
            }

            db.Psychotherapeutics.Remove(psychotherapeutic);
            db.SaveChanges();

            return Ok(psychotherapeutic);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PsychotherapeuticExists(int id)
        {
            return db.Psychotherapeutics.Count(e => e.id == id) > 0;
        }
    }
}