﻿using Medius.WebApi.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Medius.WebApi.Controllers
{
    public class BaseApiController : ApiController
    {
        protected DateTime ParseDateTime(string id)
        {
            return DateTime.ParseExact(id, MediusDateTimeConverter.DATETIME_FORMAT, CultureInfo.InvariantCulture);
        }
    }
}