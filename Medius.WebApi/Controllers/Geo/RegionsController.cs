﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class RegionsController : ApiController
    {
        private MediusContext db = new MediusContext();

        public RegionsController()
        {
            this.db.Configuration.LazyLoadingEnabled = false;
        }

        // GET: api/Regions?expression=str...
        public IQueryable<Region> GetRegions([FromUri] string expression, [FromUri] int limit = 16)
        {
            var query = from e in db.Regions
                        where e.title.ToLower().Contains(expression.ToLower())
                        select e;
            return query.Take(limit);
        }

        // GET: api/Regions
        public IQueryable<Region> GetRegions()
        {
            return db.Regions;
        }

        // GET: api/Regions/5
        [ResponseType(typeof(Region))]
        public IHttpActionResult GetRegion(int id)
        {
            Region region = db.Regions.Find(id);
            if (region == null)
            {
                return NotFound();
            }

            return Ok(region);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RegionExists(int id)
        {
            return db.Regions.Count(e => e.id == id) > 0;
        }
    }
}