﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class LocalitiesController : ApiController
    {
        private MediusContext db = new MediusContext();

        public LocalitiesController()
        {
            this.db.Configuration.LazyLoadingEnabled = false;
        }

        // GET: api/Regions/5/Localities?expression=str...
        public IQueryable<Locality> GetRegions(int regionId, [FromUri] string expression, [FromUri] int limit = 16)
        {
            var query = from e in db.Localities.Where(e => e.regionId == regionId)
                        where e.title.ToLower().Contains(expression.ToLower())
                        select e;
            return query.Take(limit);
        }

        // GET: api/Regions/5/Localities
        public IQueryable<Locality> GetLocalities(int regionId)
        {
            return db.Localities.Where(p => p.regionId == regionId);
        }

        // GET: api/Localities/5
        [ResponseType(typeof(Locality))]
        public IHttpActionResult GetLocality(int id)
        {
            Locality locality = db.Localities.Find(id);
            if (locality == null)
            {
                return NotFound();
            }

            return Ok(locality);
        }

        // PUT: api/Localities/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLocality(int id, Locality locality)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != locality.id)
            {
                return BadRequest();
            }

            db.Entry(locality).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LocalityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LocalityExists(int id)
        {
            return db.Localities.Count(e => e.id == id) > 0;
        }
    }
}