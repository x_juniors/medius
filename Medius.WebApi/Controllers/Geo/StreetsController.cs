﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class StreetsController : ApiController
    {
        private MediusContext db = new MediusContext();

        public StreetsController()
        {
            this.db.Configuration.LazyLoadingEnabled = false;
        }

        // GET: api/Localities/5/Streets?expression=str...
        public IQueryable<Street> GetStreets(int localityId, [FromUri] string expression, int limit = 16)
        {
            var query = from s in db.Streets.Where(p => p.localityId == localityId)
                        where s.title.ToLower().Contains(expression.ToLower())
                        select s;
            return query.Take(limit);
        }

        // GET: api/Localities/5/Streets
        public IQueryable<Street> GetStreets(int localityId)
        {
            return db.Streets.Where(p => p.localityId == localityId);
        }

        // GET: api/Streets/5
        [ResponseType(typeof(Street))]
        public IHttpActionResult GetStreet(int id)
        {
            Street street = db.Streets.Find(id);
            if (street == null)
            {
                return NotFound();
            }

            return Ok(street);
        }

        // PUT: api/Streets/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutStreet(int id, Street street)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != street.id)
            {
                return BadRequest();
            }

            db.Entry(street).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StreetExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Localities/5/Streets
        [ResponseType(typeof(Street))]
        public IHttpActionResult PostStreet(int localityId, Street street)
        {
            street.localityId = localityId;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Streets.Add(street);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (StreetExists(street.id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = street.id }, street);
        }

        // DELETE: api/Streets/5
        [ResponseType(typeof(Street))]
        public IHttpActionResult DeleteStreet(int id)
        {
            Street street = db.Streets.Find(id);
            if (street == null)
            {
                return NotFound();
            }

            db.Streets.Remove(street);
            db.SaveChanges();

            return Ok(street);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StreetExists(int id)
        {
            return db.Streets.Count(e => e.id == id) > 0;
        }
    }
}