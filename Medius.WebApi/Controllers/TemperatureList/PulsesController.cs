﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;
using Medius.WebApi.Converters;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class PulsesController : BaseApiController
    {
        private MediusContext db = new MediusContext();

        public PulsesController()
        {
            db.Configuration.LazyLoadingEnabled = false;
            var test = this;
        }
        // GET: api/Pulses
        public IQueryable<Pulse> GetPulses(int medcardId)
        {
            return db.Pulses.Where(p => p.medcardId == medcardId);
        }

        // GET: api/Pulses/5
        [ResponseType(typeof(Pulse))]
        public IHttpActionResult GetPulse(string id)
        {
            var dt = this.ParseDateTime(id);
            Pulse pulse = db.Pulses.Find(dt);
            if (pulse == null)
            {
                return NotFound();
            }
            
            return Ok(pulse);
        }

        // PUT: api/Pulses/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPulse(string id, Pulse pulse)
        {
            pulse.date = this.ParseDateTime(id);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Entry(pulse).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PulseExists(pulse.date))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Pulses
        [ResponseType(typeof(Pulse))]
        public IHttpActionResult PostPulse(int medcardId, Pulse pulse)
        {
            pulse.medcardId = medcardId;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Pulses.Add(pulse);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PulseExists(pulse.date))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pulse.date }, pulse);
        }

        // DELETE: api/Pulses/5
        [ResponseType(typeof(Pulse))]
        public IHttpActionResult DeletePulse(string id)
        {
            var dt = this.ParseDateTime(id);
            Pulse pulse = db.Pulses.Find(dt);
            if (pulse == null)
            {
                return NotFound();
            }

            db.Pulses.Remove(pulse);
            db.SaveChanges();

            return Ok(pulse);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PulseExists(DateTime id)
        {
            return db.Pulses.Count(e => e.date == id) > 0;
        }
    }
}