﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class PressuresController : BaseApiController
    {
        private MediusContext db = new MediusContext();

        public PressuresController()
        {
            db.Configuration.LazyLoadingEnabled = false;
        }

        // GET: api/Pressures
        public IQueryable<Pressure> GetPressures(int medcardId)
        {
            return db.Pressures.Where(p => p.medcardId == medcardId);
        }

        // GET: api/Pressures/5
        [ResponseType(typeof(Pressure))]
        public IHttpActionResult GetPressure(string id)
        {
            var dt = ParseDateTime(id);
            Pressure pressure = db.Pressures.Find(dt);
            if (pressure == null)
            {
                return NotFound();
            }

            return Ok(pressure);
        }

        // PUT: api/Pressures/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPressure(string id, Pressure pressure)
        {
            pressure.date = ParseDateTime(id);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Entry(pressure).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PressureExists(pressure.date))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Pressures
        [ResponseType(typeof(Pressure))]
        public IHttpActionResult PostPressure(int medcardId, Pressure pressure)
        {
            pressure.medcardId = medcardId;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Pressures.Add(pressure);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PressureExists(pressure.date))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pressure.date }, pressure);
        }

        // DELETE: api/Pressures/5
        [ResponseType(typeof(Pressure))]
        public IHttpActionResult DeletePressure(string id)
        {
            var dt = ParseDateTime(id);
            Pressure pressure = db.Pressures.Find(dt);
            if (pressure == null)
            {
                return NotFound();
            }

            db.Pressures.Remove(pressure);
            db.SaveChanges();

            return Ok(pressure);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PressureExists(DateTime id)
        {
            return db.Pressures.Count(e => e.date == id) > 0;
        }
    }
}