﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;
using Medius.WebApi.Converters;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class TemperaturesController : BaseApiController
    {
        private MediusContext db = new MediusContext();

        public TemperaturesController()
        {
            db.Configuration.LazyLoadingEnabled = false;
        }

        // GET: api/Temperatures
        public IQueryable<Temperature> GetTemperatures(int medcardId)
        {
            return db.Temperatures.Where(p => p.medcardId == medcardId);
        }

        // GET: api/Temperatures/5
        [ResponseType(typeof(Temperature))]
        public IHttpActionResult GetTemperature(string id)
        {
            var dt = this.ParseDateTime(id);
            Temperature temperature = db.Temperatures.Find(dt);
            if (temperature == null)
            {
                return NotFound();
            }

            return Ok(temperature);
        }

        // PUT: api/Temperatures/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTemperature(string id, Temperature temperature)
        {
            var dt = ParseDateTime(id);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (dt != temperature.date)
            {
                return BadRequest();
            }

            db.Entry(temperature).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TemperatureExists(dt))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Temperatures
        [ResponseType(typeof(Temperature))]
        public IHttpActionResult PostTemperature(int medcardId, Temperature temperature)
        {
            temperature.medcardId = medcardId;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Temperatures.Add(temperature);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (TemperatureExists(temperature.date))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = temperature.date }, temperature);
        }

        // DELETE: api/Temperatures/5
        [ResponseType(typeof(Temperature))]
        public IHttpActionResult DeleteTemperature(string id)
        {
            var dt = ParseDateTime(id);
            Temperature temperature = db.Temperatures.Find(dt);
            if (temperature == null)
            {
                return NotFound();
            }

            db.Temperatures.Remove(temperature);
            db.SaveChanges();

            return Ok(temperature);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TemperatureExists(DateTime id)
        {
            return db.Temperatures.Count(e => e.date == id) > 0;
        }
    }
}