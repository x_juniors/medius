﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "administrator")]
    public class WorkplacesController : ApiController
    {
        private MediusContext db = new MediusContext();

        // GET: api/Workplaces
        public IQueryable<Workplace> GetWorkplaces()
        {
            return db.Workplaces;
        }

        // GET: api/Workplaces/5
        [ResponseType(typeof(Workplace))]
        public IHttpActionResult GetWorkplace(int id)
        {
            Workplace workplace = db.Workplaces.Find(id);
            if (workplace == null)
            {
                return NotFound();
            }

            return Ok(workplace);
        }

        // PUT: api/Workplaces/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutWorkplace(int id, Workplace workplace)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != workplace.id)
            {
                return BadRequest();
            }

            db.Entry(workplace).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WorkplaceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Workplaces
        [ResponseType(typeof(Workplace))]
        public IHttpActionResult PostWorkplace(Workplace workplace)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Workplaces.Add(workplace);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = workplace.id }, workplace);
        }

        // DELETE: api/Workplaces/5
        [ResponseType(typeof(Workplace))]
        public IHttpActionResult DeleteWorkplace(int id)
        {
            Workplace workplace = db.Workplaces.Find(id);
            if (workplace == null)
            {
                return NotFound();
            }

            db.Workplaces.Remove(workplace);
            db.SaveChanges();

            return Ok(workplace);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool WorkplaceExists(int id)
        {
            return db.Workplaces.Count(e => e.id == id) > 0;
        }
    }
}