﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.BLL.Services;
using Medius.DAL.DataModels;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class AccountsController : ApiController
    {
        private IAccountService _accountService;

        public AccountsController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        // GET: api/accounts
        [Authorize(Roles = "administrator")]
        public IEnumerable<PublicAccountModel> GetAccounts()
        {
            return _accountService.Get();
        }

        // GET: api/accounts/5
        [ResponseType(typeof(PublicAccountModel))]
        public IHttpActionResult GetAccount(int id)
        {
            var account = _accountService.Get(id);
            if (account == null)
            {
                return NotFound();
            }
            return Ok(account);
        }

        // PUT: api/Accounts/5
        [Route("api/accounts/{id}/password")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAccountPassword(int id, PasswordModel password)
        {
            password.id = id;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                _accountService.ChangePassword(password); // clear tokens
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // PUT: api/accounts/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAccount(int id, PublicAccountModel account)
        {
            account.id = id;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                _accountService.Update(account);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Accounts
        [Authorize(Roles = "administrator")]
        [ResponseType(typeof(AccountModel))]
        public IHttpActionResult PostAccount(AccountModel account)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                account = _accountService.Register(account);
            }
            catch (DbUpdateException)
            {
                if (AccountExists(account.id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = account.id }, account);
        }

        // DELETE: api/Accounts/5
        [Authorize(Roles = "administrator")]
        [ResponseType(typeof(PublicAccountModel))]
        public IHttpActionResult DeleteAccount(int id)
        {
            var account = _accountService.Remove(id);
            if (account == null)
            {
                return NotFound();
            }
            return Ok(account);
        }

        private bool AccountExists(int id)
        {
            return _accountService.Exist(id);
        }
    }
}