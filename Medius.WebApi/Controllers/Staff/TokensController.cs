﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web;
using Medius.BLL.Services;
using Medius.DAL.DataModels;
using Medius.WebApi.Security;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "nurse")]
    public class TokensController : ApiController
    {
        private IAccountService _accountService;
        private ITokenFormationService _tokenFormationService;

        public TokensController(IAccountService accountService, ITokenFormationService tokenFormationService)
        {
            _accountService = accountService;
            _tokenFormationService = tokenFormationService;
        }

        // GET: api/accounts/5/tokens
        public IEnumerable<TokenModel> GetTokens(int accountId)
        {
            return _accountService.GetAllTokens(accountId);
        }

        // POST: api/tokens
        [AllowAnonymous]
        [ResponseType(typeof(TokenModel))]
        public IHttpActionResult PostToken(AccountModel credentials)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var token = _tokenFormationService.ExtractToken(Request);
            token = _accountService.AddToken(token, credentials.login, credentials.passwordHash);
            if (token == null)
            {
                return BadRequest("Login/Password incorrect");
            }
            var account = _accountService.Get(token.accountId);
            if (account == null)
            {
                return BadRequest("Login/Password incorrect");
            }
            return CreatedAtRoute("DefaultApi",
                new { id = token.value },
                new { token = token, role = account.Role.roleTitle, accountId = account.id }
            );
        }

        // DELETE: api/tokens/5
        [ResponseType(typeof(TokenModel))]
        public IHttpActionResult DeleteToken(Guid id)
        {
            var token = _accountService.RemoveToken(id);
            if (token == null)
            {
                return NotFound();
            }
            return Ok(token);
        }

        // DELETE: api/account/5/tokens
        [ResponseType(typeof(TokenModel))]
        public IHttpActionResult DeleteToken(int accountId)
        {
            var tokenValue = _tokenFormationService.ExtractTokenValue(Request);
            var token = _accountService.ClearAllExcept(accountId, tokenValue);
            if (token == null)
            {
                return NotFound();

            }
            return Ok(token);
        }
    }
}