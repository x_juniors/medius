﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [Authorize(Roles = "administrator")]
    public class SeparationsController : ApiController
    {
        private MediusContext db = new MediusContext();

        public SeparationsController()
        {
            db.Configuration.LazyLoadingEnabled = false;
        }

        // GET: api/Separations
        public IQueryable<Separation> GetSeparations()
        {
            return db.Separations;
        }

        // GET: api/Separations/5
        [ResponseType(typeof(Separation))]
        public IHttpActionResult GetSeparation(int id)
        {
            Separation separation = db.Separations.Find(id);
            if (separation == null)
            {
                return NotFound();
            }

            return Ok(separation);
        }

        // PUT: api/Separations/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSeparation(int id, Separation separation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != separation.id)
            {
                return BadRequest();
            }

            db.Entry(separation).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SeparationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Separations
        [ResponseType(typeof(Separation))]
        public IHttpActionResult PostSeparation(Separation separation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Separations.Add(separation);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = separation.id }, separation);
        }

        // DELETE: api/Separations/5
        [ResponseType(typeof(Separation))]
        public IHttpActionResult DeleteSeparation(int id)
        {
            Separation separation = db.Separations.Find(id);
            if (separation == null)
            {
                return NotFound();
            }

            db.Separations.Remove(separation);
            db.SaveChanges();

            return Ok(separation);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SeparationExists(int id)
        {
            return db.Separations.Count(e => e.id == id) > 0;
        }
    }
}