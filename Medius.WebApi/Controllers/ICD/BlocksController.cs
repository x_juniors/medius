﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [AllowAnonymous]
    public class BlocksController : ApiController
    {
        private MediusContext db = new MediusContext();

        // GET: api/Blocks
        public IQueryable<Block> GetBlocks(int icdclassId)
        {
            return db.Blocks.Where(p => p.icdClassId == icdclassId);
        }

        // GET: api/Blocks/5
        [ResponseType(typeof(Block))]
        public IHttpActionResult GetBlock(string id)
        {
            Block block = db.Blocks.Find(id);
            if (block == null)
            {
                return NotFound();
            }

            return Ok(block);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}