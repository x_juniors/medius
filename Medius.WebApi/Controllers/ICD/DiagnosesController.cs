﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [AllowAnonymous]
    public class DiagnosesController : ApiController
    {
        private MediusContext db = new MediusContext();

        // GET: api/Diagnoses
        public IQueryable<Diagnose> GetDiagnoses(string blockId)
        {
            return db.Diagnoses.Where(p => p.blockId == blockId);
        }

        // GET: api/Diagnoses/5
        [ResponseType(typeof(Diagnose))]
        public IHttpActionResult GetDiagnose(string id)
        {
            Diagnose diagnose = db.Diagnoses.Find(id);
            if (diagnose == null)
            {
                return NotFound();
            }

            return Ok(diagnose);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}