﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Medius.DAL.EDM;

namespace Medius.WebApi.Controllers
{
    [AllowAnonymous]
    public class IcdClassesController : ApiController
    {
        private MediusContext db = new MediusContext();

        // GET: api/IcdClasses
        public IQueryable<IcdClass> GetIcdClasses()
        {
            return db.IcdClasses;
        }

        // GET: api/IcdClasses/5
        [ResponseType(typeof(IcdClass))]
        public IHttpActionResult GetIcdClass(int id)
        {
            IcdClass icdClass = db.IcdClasses.Find(id);
            if (icdClass == null)
            {
                return NotFound();
            }

            return Ok(icdClass);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}