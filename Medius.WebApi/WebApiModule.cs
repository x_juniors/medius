﻿using Autofac;
using Autofac.Integration.WebApi;
using Medius.BLL;
using Medius.WebApi.Security;

namespace Medius.WebApi
{
    public class WebApiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<BLLModule>();
            builder.RegisterApiControllers(System.Reflection.Assembly.GetExecutingAssembly());
            builder.RegisterType<TokenFormationService>().As<ITokenFormationService>();
            builder.RegisterType<AuthenticationService>().As<IAuthenticationService>();
            builder.RegisterType<MediusAuthorizeAttribute>().AsSelf();
        }
    }
}