﻿using Autofac;
using Autofac.Integration.WebApi;
using Medius.WebApi.Converters;
using Medius.WebApi.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Optimization;
using System.Web.Routing;

namespace Medius.WebApi
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            var builder = new Autofac.ContainerBuilder();
            builder.RegisterModule<WebApiModule>();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            var container = builder.Build();

            var config = GlobalConfiguration.Configuration;
            var formatters = config.Formatters;
            var settings = formatters.JsonFormatter.SerializerSettings;
            settings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            settings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.None;
            settings.Formatting = Newtonsoft.Json.Formatting.Indented;
            settings.ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();
            formatters.XmlFormatter.SupportedMediaTypes.Clear();

            settings.Converters.Add(new MediusDateTimeConverter());
            config.Filters.Add(container.Resolve<MediusAuthorizeAttribute>());
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}
